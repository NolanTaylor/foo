extends Camera2D

const VELOCITY = 420

var lookin : bool = false
var target_pos : Vector2
var velocity : Vector2
var cam_area : Rect2

func bacc() -> void:
	lookin = false

func _ready():
	pass
	
func _process(delta):
	if lookin:
		position += velocity * delta
	
func run_inputs(player_pos : Vector2, area : Rect2, \
direction : String = "around", _zoom : float = 1.0, \
follow : bool = true):
	if visible:
		if Input.is_action_pressed("cam_left"):
			lookin = true
			velocity.x = -VELOCITY
		elif Input.is_action_pressed("cam_right"):
			lookin = true
			velocity.x = VELOCITY
		else:
			velocity.x = lerp(velocity.x, 0, 0.20)
			
		if Input.is_action_pressed("cam_up"):
			lookin = true
			velocity.y = -VELOCITY
		elif Input.is_action_pressed("cam_down"):
			lookin = true
			velocity.y = VELOCITY
		else:
			velocity.y = lerp(velocity.y, 0, 0.2)
		
		if follow and !lookin:
			position.x = player_pos.x - 512
			position.y = player_pos.y - 300
			
			match (direction):
				"north":
					position.y -= 150
				"east":
					position.x += 256
				"south":
					position.y += 150
				"west":
					position.x -= 256
				"around":
					pass
		
		if _zoom != 1.0:
			self.zoom = Vector2(_zoom, _zoom)
				
		if position.x < area.position.x:
			position.x = area.position.x
		if position.y < area.position.y:
			position.y = area.position.y
		if position.x + 1024 > area.position.x + area.size.x:
			position.x = area.position.x + area.size.x - 1024
		if position.y + 600 > area.position.y + area.size.y:
			position.y = area.position.y + area.size.y - 600
				
