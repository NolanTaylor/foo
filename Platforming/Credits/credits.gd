extends Node2D

onready var tween_out = get_node("tween")

var GAME_FINALLY_DONEEEEEEEEEEEEE : bool = false

func _ready():
	if get_node("/root/Config").light_mode:
		VisualServer.set_default_clear_color(Color("ffffff"))
	$timer.one_shot = true
	$timer.start(68)
	$music.play()
	for child in $texts_inactive.get_children():
		child.connect("timer_off", self, "activate")
	
func _process(delta):
	if $timer.is_stopped() and !GAME_FINALLY_DONEEEEEEEEEEEEE:
		$canvasLayer/colorRect.color.a += 0.005
		if $canvasLayer/colorRect.color.a > 1.0:
			$canvasLayer/colorRect.color.a = 1.0
			tween_out.interpolate_property($music, "volume_db", \
				-8.0, -80, 6.0, 1, Tween.EASE_IN, 0)
			tween_out.start()
			GAME_FINALLY_DONEEEEEEEEEEEEE = true
	
func activate(child : Label):
	var last_child_match : bool = false
	child.init()
	if child.color == Color("26252a"):
		$receive.play()
	else:
		$send.play()
	if $texts_active.get_child_count():
		if child.color == $texts_active.get_child( \
		$texts_active.get_child_count() - 1).color:
			last_child_match = true
			$texts_active.get_child( \
				$texts_active.get_child_count() - 1). \
					get_node("tail").hide()
	for grandchild in $texts_active.get_children():
		if last_child_match:
			grandchild.move_up(child.get_size().y + 4)
		else:
			grandchild.move_up(child.get_size().y + 12)
	$texts_inactive.remove_child(child)
	$texts_active.add_child(child)
