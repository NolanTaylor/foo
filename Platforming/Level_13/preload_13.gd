extends Node

# okay, so for some reason the sprite under the post node
# doesn't align with the parent home node, so to center it
# correctly the sprite (offset center) must be at
# vector2(768, 486)

var max_scroll : int = 8000

var posts : Array

var post_1 = preload("res://Global/InstaScenes/post.tscn")
var post_2 = preload("res://Global/InstaScenes/post.tscn")
var post_3 = preload("res://Global/InstaScenes/post.tscn")
var post_4 = preload("res://Global/InstaScenes/post.tscn")
var post_5 = preload("res://Global/InstaScenes/post.tscn")

var post_1_sprite = load( \
"res://Assets/Photos/11_1.png")
var post_2_sprite = load( \
"res://Assets/Photos/11_2.png")
var post_3_sprite = load( \
"res://Assets/Photos/11_3.png")
var post_4_sprite = load( \
"res://Assets/Photos/11_4.png")
var post_5_sprite = load( \
"res://Assets/Photos/11_5.png")

var post_1_poster = "Bar"
var post_2_poster = "Hana"
var post_3_poster = "Alissa"
var post_4_poster = "Ashe"
var post_5_poster = "NULL"

var post_1_views = "21 views"
var post_2_views = "3 views"
var post_3_views = "12 views"
var post_4_views = "16 views"
var post_5_views = "0 views"

var post_1_description = "trans people are people"
var post_2_description = "0-0"
var post_3_description = "Just failed english"
var post_4_description = "falling apart, but at least i look pretty doing it"
var post_5_description = "This is placeholder text: YEE HAW"

var post_1_time = "3 hours ago"
var post_2_time = "2 hours ago"
var post_3_time = "4 hours ago"
var post_4_time = "1 hours ago"
var post_5_time = "0 hours ago"

func _ready():
	posts = [
		[post_1, post_1_sprite, post_1_poster, \
		post_1_views, post_1_description, post_1_time],
		[post_2, post_2_sprite, post_2_poster, \
		post_2_views, post_2_description, post_2_time],
		[post_3, post_3_sprite, post_3_poster, \
		post_3_views, post_3_description, post_3_time],
		[post_4, post_4_sprite, post_4_poster, \
		post_4_views, post_4_description, post_4_time],
		[post_5, post_5_sprite, post_5_poster, \
		post_5_views, post_5_description, post_5_time],
	]
