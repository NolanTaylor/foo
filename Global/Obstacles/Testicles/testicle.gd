extends Area2D

const EXTEND : float = 0.4

var player_body
var active : bool = false
var ready : bool = false
var spawned : bool = false
var finished : bool = false
var has_reset : bool = false
var despawn : bool = false
var spawn_time : float = EXTEND
var despawn_time : float = 1.5
var type : String = "platforming"

signal death
signal despawned

func set_body(body):
	player_body = body
	
func set_time(spawn, despawn):
	spawn_time = spawn
	despawn_time = despawn
	
func set_type(x : String):
	type = x
	
func spawn():
	show()
	$spawn_timer.start(spawn_time)
	spawned = true
	
func reset():
	hide()
	$tentacle_sprite.frame = 0
	active = false
	ready = false
	spawned = false
	finished = false
	has_reset = true
	
	if type == "thad":
		emit_signal("despawned")

func _ready():
	hide()
	$spawn_timer.one_shot = true
	$despawn_timer.one_shot = true

func _process(delta):
	if overlaps_body(player_body) and active:
		emit_signal("death")
		
	match type:
		"platforming":
			run_platforming()
		"thad":
			run_thad()
			
func run_platforming():
	if !finished:
		if $spawn_timer.is_stopped() and ready:
			spawned = true
			ready = false
			active = true
			$despawn_timer.start(despawn_time)
			
		if $despawn_timer.is_stopped() and spawned and despawn:
			hide()
			finished = true
			active = false
				
	if $trigger.overlaps_body(player_body) and !spawned and !finished:
		if has_reset:
			has_reset = false
		elif !ready:
			show()
			$spawn_timer.start(spawn_time)
			$tentacle_sprite.play("spawn")
			ready = true
	
func run_thad():
	if spawned:
		$tentacle_sprite.play("idle")
		
		if $spawn_timer.is_stopped():
			$spawn_timer.start(EXTEND)
			spawned = false
			ready = true
	elif ready:
		$tentacle_sprite.play("spawn")
		
		if $spawn_timer.is_stopped():
			$despawn_timer.start(despawn_time)
			ready = false
			finished = true
			active = true
	elif finished:
		if $despawn_timer.is_stopped():
			active = false
			$tentacle_sprite.play("spawn", true)
			if $tentacle_sprite.frame == 1:
				reset()

func _on_tentacle_sprite_animation_finished():
	if $tentacle_sprite.animation == "spawn":
		$tentacle_sprite.frame = 4
		$tentacle_sprite.stop()
