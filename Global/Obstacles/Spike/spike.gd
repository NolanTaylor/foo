extends Area2D

var player_body = null

signal death

func set_body(body):
	player_body = body

func _ready():
	pass

func _process(delta):
	if overlaps_body(player_body):
		emit_signal("death")
