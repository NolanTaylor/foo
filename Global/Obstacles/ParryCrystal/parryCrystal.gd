extends Area2D

var player_body

signal collect

func set_body(body):
	player_body = body
	
func reset():
	show()
	$collisionShape.disabled = false
	
func _ready():
	pass
	
func _process(delta):
	if overlaps_body(player_body):
		hide()
		$collisionShape.disabled = true
		emit_signal("collect")
