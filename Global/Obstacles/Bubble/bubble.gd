extends KinematicBody2D

const BB = 260 # i've already forgotten what BB stands for

var velocity : Vector2
var rest : Vector2
var player_body

signal bounce(v)
signal collision(collision, position)

func set_body(body):
	player_body = body
	
func reset():
	position = rest
	
	if has_node("trigger"):
		hide()
		$collisionShape.disabled = true
	
func parried(body, pos):
	if body == self:
		var v : Vector2 = Vector2( \
		pos.x - position.x, pos.y - position.y)
		var v_add : Vector2 = Vector2(0, 0)
		var sine : float = v.y / sqrt(pow(v.x, 2) + pow(v.y, 2))
		
		if v.x >= 0:
			if sine <= -0.75:
				v_add = Vector2(0, -BB)
			elif sine <= -0.25:
				v_add = Vector2(BB / 1.25, -BB / 1.25)
			elif sine <= 0.25:
				v_add = Vector2(BB, 0)
			elif sine <= 0.75:
				v_add = Vector2(BB / 1.25, BB / 1.25)
			else:
				v_add = Vector2(0, BB)
		elif v.x < 0:
			if sine >= 0.75:
				v_add = Vector2(0, BB)
			elif sine >= 0.25:
				v_add = Vector2(-BB / 1.25, BB / 1.25)
			elif sine >= -0.25:
				v_add = Vector2(-BB, 0)
			elif sine >= -0.75:
				v_add = Vector2(-BB / 1.25, -BB / 1.25)
			else:
				v_add = Vector2(0, -BB)
				
		velocity = -v_add
		
		emit_signal("bounce", v_add)

func _ready():
	rest = position
	
	if has_node("trigger"):
		hide()
		$collisionShape.disabled = true
	
func _physics_process(delta):
	var collision = move_and_collide(velocity * delta)
	if collision:
		if max(abs(velocity.x), abs(velocity.y)):
			velocity *= 260 / max(abs(velocity.x), abs(velocity.y))
		velocity = velocity.bounce(collision.normal)
		emit_signal("collision", collision.collider, collision.position)
	velocity = lerp(velocity, Vector2(0, 0), 0.1)
	
	if has_node("trigger"):
		if $trigger.overlaps_body(player_body):
			show()
			$collisionShape.disabled = false
