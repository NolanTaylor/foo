extends Node2D

const WHITE : Color = Color(255, 255, 255)

var length : int
var height : int

func set_dimensions():
	length = get_parent().length
	height = get_parent().height

func _ready():
	pass

func _process(delta):
	pass
	
func _draw():
	var lpos : Vector2 = Vector2(-length * 4, -(height - 1) * 8)
	var hpos : Vector2 = Vector2(-(length - 2) * 4, -height * 8)
	var lsiz : Vector2 = Vector2(length * 8, (height - 1) * 16)
	var hsiz : Vector2 = Vector2((length - 2) * 8, height * 16)
	
	draw_rect(Rect2(lpos, lsiz), WHITE)
	draw_rect(Rect2(hpos, hsiz), WHITE)
	
	draw_circle(Vector2((length - 2) * 4, -(height - 1) * 8), 8, WHITE)
	draw_circle(Vector2(-(length - 2) * 4, -(height - 1) * 8), 8, WHITE)
	draw_circle(Vector2(-(length - 2) * 4, (height - 1) * 8), 8, WHITE)
	draw_circle(Vector2((length - 2) * 4, (height - 1) * 8), 8, WHITE)
