extends Label

const BLUE : Color = Color("147efb")
const GREEN : Color = Color("43CC47")
const GRAY : Color = Color("26252a")
const LIGHT_GRAY : Color = Color("e9e9eb")

var radius : int = 16
var act : int = 0
var size : Vector2
var target_pos : Vector2
var color : Color

signal timer_off(child)

func init():
	show()
	rect_position.y = 600
	$actual_label.text = text
	$actual_label.rect_position.x += 12
	text = ""
	if rect_position.x < 384:
		if get_node("/root/Config").light_mode:
			color = LIGHT_GRAY
			$actual_label.set("custom_colors/font_color", \
				Color("000000"))
		else:
			color = GRAY
		$tail.flip_h = false
		$tail.position = Vector2(-11, get_size().y - 16)
	else:
		color = BLUE
		$tail.flip_h = true
		$tail.position = Vector2(get_size().x - 10, get_size().y - 16)
	$tail.modulate = color
	
func move_up(distance : int):
	target_pos.y -= distance
	act = 2
	
func _ready():
	$timer.one_shot = true
	
	hide()
	
	size = Vector2(get_size().x, get_size().y)
	target_pos = rect_position
	
func _process(delta):
	match act:
		0:
			$timer.start(int($data.editor_description))
			act += 1
		1:
			if $timer.is_stopped():
				emit_signal("timer_off", self)
				act += 1
		2:
			rect_position.y = lerp(rect_position.y, target_pos.y, 0.1)
			
			if rect_position.y <= target_pos.y + 1:
				rect_position.y = target_pos.y
				act += 1
		3:
			pass
	
func _draw():
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = color
	style_box.set_corner_radius_all(radius)
	draw_style_box(style_box, Rect2(Vector2(0, 0), size))
