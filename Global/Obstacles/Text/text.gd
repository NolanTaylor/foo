extends StaticBody2D

const SPAWN_MARGIN : int = 50
const MAX_LENGTH : int = 140
const VELOCITY : int = 280
const KEVIN : int = 360
const ACCELERATION : int = 12
const DELIVER_TIME : float = 0.8
const READ_TIME : float = 0.8
const SQUIRCL_DIMENSIONS : Vector2 = Vector2(384, 384)
const BLUE : Color = Color("147efb")
const GREEN : Color = Color("43CC47")
const GRAY : Color = Color("26252a")
const LIGHT_GRAY : Color = Color("e9e9eb")

var player_body
var flip : bool = false
var reverse : bool = false
var disappearing : bool = false
var traveling : bool = false
var existence : bool = true
var delivered : bool = true
var read : bool = false
var tail : bool = false
var act : int = 0
var length : int
var height : int = 1
var radius : int
var spd : int
var target_x : int
var delay : float = 0.0
var color : Color
var sender : String
var target_pos : Vector2
var velocity : Vector2
var size : Vector2
var spike_arrays : Array
var warbling : Array = [false, "null"]
var rest_points : PoolVector2Array = [Vector2(0, 0), Vector2(0, 0)]
var font : DynamicFont = load("res://Assets/Fonts/sanFrancisco.tres")

signal death
signal appear(sent)

func set_body(body):
	player_body = body
	
func reset():
	hide()
	$read.hide()
	$collisionShape.shape.extents = Vector2(0, 0)
	$collisionShape.position = Vector2(0, 0)
	$collisionShape.disabled = true
	$trigger/collisionShape.disabled = false
	if has_node("spikes"):
		spike_arrays.clear()
		for child in $spikes.get_children():
			child.disabled = true
	disappearing = false
	read = false
	existence = true
	act = 0
	length = 0
	height = 1
	modulate.a = 1
	position = target_pos
	velocity = Vector2(0, 0)
	
func init():
	show()
	
	$collisionShape.disabled = false
	
	length = $label.get_size().x
	height = $label.get_size().y
	
	$collisionShape.position += Vector2(length / 2, height / 2)
	$collisionShape.shape.extents = Vector2(length / 2, height / 2)
	
	$label.rect_position = Vector2(12, 0)
	
	$deliver_timer.start(DELIVER_TIME)
	
	size = Vector2(length, height)
	radius = 16
	act = 2
	
	if flip:
		position.y = target_pos.y - SPAWN_MARGIN
	else:
		position.y = target_pos.y + SPAWN_MARGIN
	
	if sender == "foo":
		color = BLUE
		delivered = false
		emit_signal("appear", true)
		$read.set_position(Vector2(length - 50, height + 4))
		$tail.flip_h = true
		$tail.modulate = BLUE
		$tail.position = Vector2(length - 10, height - 16)
		$tail.show()
		tail = true
	elif sender == "foo_null":
		color = BLUE
	elif sender == "foo_gc":
		color = GREEN
		emit_signal("appear", true)
		$tail.flip_h = true
		$tail.modulate = GREEN
		$tail.position = Vector2(length - 10, height - 16)
		$tail.show()
		tail = true
	elif sender == "foo_gc_null":
		color = GREEN
	else:
		if get_node("/root/Config").light_mode:
			color = LIGHT_GRAY
			$label.set("custom_colors/font_color", Color(0,0,0))
		else:
			color = GRAY
		
		if sender != "null":
			if sender != "nina":
				$name.show()
				$name.set_position(Vector2(12, -16))
				if sender == "alissa":
					sender = "kerry"
				$name.text = sender
			emit_signal("appear", false)
			if get_node("/root/Config").light_mode:
				$tail.modulate = LIGHT_GRAY
			else:
				$tail.modulate = GRAY
			$tail.position = Vector2(-11, height - 16)
			$tail.show()
			tail = true
			
	if has_node("spikes"):
		for child in $spikes.get_children():
			child.disabled = false
			
			poolArray_convert(child.position, child.shape.extents)
	elif has_node("fake_spikes"):
		for child in $fake_spikes.get_children():
			poolArray_convert(child.position, child.shape.extents)
	
	if has_node("disappear"):
		for child in $disappear.get_children():
			bloom_convert(child.position, child.shape.extents)
			
	if has_node("kevin"):
		$kevin/collisionShape.position = \
		Vector2(length / 2, height / 2)
		$kevin/collisionShape.shape.extents = \
		Vector2(length / 2 + 12, height / 2 + 12)
			
	if has_node("translate"):
		rest_points[0] = $translate.points[0] + target_pos
		rest_points[1] = $translate.points[1] + target_pos
			
	if has_node("phase"):
		if $phase.editor_description == "true":
			existence = false
		else:
			existence = true
			
		phase()
	
func poolArray_convert(pos : Vector2, size : Vector2):
	var face : String
	var spike : PoolVector2Array
	
	if pos.x < 0:
		face = "west"
	elif pos.y < 0:
		face = "north"
	elif pos.x > $label.get_size().x:
		face = "east"
	else:
		face = "south"
		
	match face:
		"north":
			for i in range(0, 2 * size.x + 8, 8):
				if !i:
					spike.append(Vector2(pos.x - size.x, 10))
				elif i % 16:
					spike.append(Vector2(pos.x - size.x + i, -32))
				else:
					spike.append(Vector2(pos.x - size.x + i, 0))
			spike.append(Vector2(pos.x + size.x, 10))
			$name.set_position(Vector2(12, -48))
		"east":
			for i in range(0, 2 * size.y + 8, 8):
				if !i:
					spike.append(Vector2(pos.x - 18, pos.y - size.y))
				elif i % 16:
					spike.append(Vector2(pos.x + 24, \
					pos.y - size.y + i))
				else:
					spike.append(Vector2(pos.x - 8, \
					pos.y - size.y + i))
			spike.append(Vector2(pos.x - 18, pos.y + size.y))
		"south":
			for i in range(0, 2 * size.x + 8, 8):
				if !i:
					spike.append(Vector2(pos.x - size.x, \
					pos.y - 18))
				elif i % 16:
					spike.append(Vector2(pos.x - size.x + i,
					pos.y + 24))
				else:
					spike.append(Vector2(pos.x - size.x + i, \
					pos.y - 8))
			spike.append(Vector2(pos.x + size.x, pos.y - 18))
			$tail.hide()
			tail = false
			$read.set_position(Vector2(length - 50, height + 36))
		"west":
			for i in range(0, 2 * size.y + 8, 8):
				if !i:
					spike.append(Vector2(10, pos.y - size.y))
				elif i % 16:
					spike.append(Vector2(-32, pos.y - size.y + i))
				else:
					spike.append(Vector2(0, pos.y - size.y + i))
			spike.append(Vector2(10, pos.y + size.y))
			$tail.hide()
			tail = false
					
	spike_arrays.append(spike)
	
func bloom_convert(pos, size):
	var face : String
	
	if pos.x < 0:
		face = "west"
	elif pos.y < 0:
		face = "north"
	elif pos.x > $label.get_size().x:
		face = "east"
	else:
		face = "south"
		
	$bloom.show()
	$bloom.position = pos
		
	match face:
		"north":
			$bloom.rotation = PI/2
			$bloom.scale.y = size.x / 2
		"east":
			$bloom.rotation = PI
			$bloom.scale.y = size.y / 2
		"south":
			$bloom.rotation = 3*PI/2
			$bloom.scale.y = size.x / 2
		"west":
			$bloom.rotation = 0
			$bloom.scale.y = size.y / 2
	
func move_up(new_text_height : int, follow_up : bool):
	target_pos.y -= 2 + new_text_height
	
#	if !follow_up:
#		target_pos.y -= FOLLOW_MARGIN
	
func move(new_pos : int, speed : int) -> void:
	target_x = new_pos
	spd = speed
	traveling = true
	
func disappear():
	disappearing = true
	$collisionShape.disabled = true
	existence = false
	if has_node("spikes"):
		$spikes/collisionShape.disabled = true
		
func kevin_warble(direction : String) -> void:
	warbling[0] = true
	warbling[1] = direction
	
func _on_kevin_body_entered(body):
	if body != player_body and body.existence:
		warbling[0] = false
	
func phase():
	if existence:
		$tail.hide()
		$collisionShape.disabled = true
		if has_node("spikes"):
			for child in $spikes.get_children():
				child.disabled = true
		existence = false
	else:
		if tail:
			$tail.show()
		$collisionShape.disabled = false
		if has_node("spikes"):
			for child in $spikes.get_children():
				child.disabled = false
		existence = true
		
	update()
	
func bounced(col_collider, col_position : Vector2):
	if col_collider == self:
		var rects : Array
		if has_node("disappear"):
			for child in $disappear.get_children():
				rects.append(Rect2(
					child.global_position.x - child.shape.extents.x,
					child.global_position.y - child.shape.extents.y,
					child.shape.extents.x * 2,
					child.shape.extents.y * 2
				))
			
			for rect in rects:
				if rect.has_point(col_position):
					disappear()
					
		if has_node("kevin"):
			var bodies : Array = $kevin.get_overlapping_bodies()
			var shapes : Array
			
			bodies.erase(player_body)
			
			for body in bodies:
				shapes.append([
					body.position,
					body.position + \
					Vector2(body.length, body.height),
				])
				
			var flag : bool = false
			
			if col_position.y >= position.y + height - 2:
				for shape in shapes:
					if shape[1].y <= position.y + 6:
						flag = true
				if !flag:
					kevin_warble("north")
			elif col_position.y <= position.y + 2:
				for shape in shapes:
					if shape[0].y >= position.y + height - 6:
						flag = true
				if !flag:
					kevin_warble("south")
			elif col_position.x <= position.x + 2:
				for shape in shapes:
					if shape[0].x >= position.x + length - 6:
						flag = true
				if !flag:
					kevin_warble("east")
			elif col_position.x >= position.x + length - 2:
				for shape in shapes:
					if shape[1].x < position.x + 6:
						flag = true
				if !flag:
					kevin_warble("west")
		elif !has_node("translate"):
			if col_position.y >= position.y + height - 2:
				velocity.y = -60
			elif col_position.y <= position.y + 2:
				velocity.y = 60
			elif col_position.x <= position.x + 2:
				velocity.x = 60
			elif col_position.x >= position.x + length - 2:
				velocity.x = -60
	
func _ready():
	hide()
	$bloom.hide()
	$collisionShape.disabled = true
	$delay_timer.one_shot = true
	$deliver_timer.one_shot = true
	target_pos = position
	
func _process(delta):
	match act:
		-1:
			pass
		0:
			if $trigger.overlaps_body(player_body):
				$delay_timer.start(delay)
				act += 1
				
				if !delay:
					init()
		1:
			if $delay_timer.is_stopped():
				init()
		2:
			position.y = lerp(position.y, target_pos.y, 0.1)
			
			if !flip and position.y <= target_pos.y + 1:
				position.y = target_pos.y
				act += 1
			elif flip and position.y >= target_pos.y -1:
				position.y = target_pos.y
				act += 1
		3:
			if has_node("spikes"):
				if $spikes.overlaps_area( \
				player_body.get_node("death_detector")):
					if !get_node("/root/Config").invincibility:
						emit_signal("death")
					
			if has_node("translate"):
				position += velocity * delta
				
				if $translate.editor_description == "vertical":
					if position.y <= rest_points[0].y and \
					position.y <= rest_points[1].y:
						velocity.y = lerp(velocity.y, VELOCITY, 0.05)
						reverse = false
					elif position.y >= rest_points[0].y and \
					position.y >= rest_points[1].y:
						velocity.y = lerp(velocity.y, -VELOCITY, 0.05)
						reverse = true
					else:
						if reverse:
							velocity.y = -VELOCITY
						else:
							velocity.y = VELOCITY
				elif $translate.editor_description == "horizontal":
					if position.x <= rest_points[0].x and \
					position.x <= rest_points[1].x:
						velocity.x = lerp(velocity.x, VELOCITY, 0.05)
						reverse = false
					elif position.x >= rest_points[0].x and \
					position.x >= rest_points[1].x:
						velocity.x = lerp(velocity.x, -VELOCITY, 0.05)
						reverse = true
					else:
						if reverse:
							velocity.x = -VELOCITY
						else:
							velocity.x = VELOCITY
			elif has_node("kevin"):
				position += velocity * delta
				
				if warbling[0]:
					match warbling[1]:
						"north":
							velocity.y = \
							max(velocity.y - ACCELERATION, -KEVIN)
						"south":
							velocity.y = \
							min(velocity.y + ACCELERATION, KEVIN)
						"east":
							velocity.x = \
							min(velocity.x + ACCELERATION, KEVIN)
						"west":
							velocity.x = \
							max(velocity.x - ACCELERATION, -KEVIN)
				else:
					velocity = Vector2(0, 0)
			else:
				position += velocity * delta
				velocity.x = lerp(velocity.x, 0, 0.05)
				velocity.y = lerp(velocity.y, 0, 0.05)
				position.x = lerp(position.x, target_pos.x, 0.1)
				position.y = lerp(position.y, target_pos.y, 0.1)
					
			if disappearing:
				modulate.a -= 0.05
				
				if modulate.a <= 0:
					act = -1
					
			if $deliver_timer.is_stopped() and !delivered:
				$read.show()
				$deliver_timer.start(READ_TIME)
				delivered = true
#			elif $deliver_timer.is_stopped() and !read:
#				$read.text = "Read [time]"
#				read = true
					
			if traveling:
				position.x += spd * delta
				
				if spd < 0:
					if global_position.x < target_x:
						traveling = false
				else:
					if global_position.x > target_x:
						traveling = false
		4:
			pass
			
func pause(col_collider, col_position : Vector2):
	if col_collider == self:
		velocity = Vector2(0, 0)
		
func _draw():
	if existence:
		var style_box : StyleBoxFlat = StyleBoxFlat.new()
		style_box.bg_color = color
		style_box.set_corner_radius_all(radius)
		draw_style_box(style_box, Rect2(Vector2(0, 0), size))
		for array in spike_arrays:
			draw_colored_polygon(array, color)
			
		if has_node("kevin"):
			var outline : StyleBoxFlat = StyleBoxFlat.new()
			outline.draw_center = false
			outline.border_color = Color("ff00ea")
			outline.border_width_bottom = 1
			outline.border_width_top = 1
			outline.border_width_left = 1
			outline.border_width_right = 1
			outline.set_corner_radius_all(radius)
			draw_style_box(outline, Rect2(Vector2(0, 0), size))
	else:
		var outline : StyleBoxFlat = StyleBoxFlat.new()
		outline.draw_center = false
		if get_node("/root/Config").light_mode:
			outline.border_color = Color("000000")
		else:
			outline.border_color = Color("ffffff")
		outline.border_width_bottom = 1
		outline.border_width_top = 1
		outline.border_width_left = 1
		outline.border_width_right = 1
		outline.set_corner_radius_all(radius)
		draw_style_box(outline, Rect2(Vector2(0, 0), size))
		
		for array in spike_arrays:
			var poly_color : Color = Color("ffffff")
			if get_node("/root/Config").light_mode:
				poly_color = Color("000000")
			draw_polyline(array, poly_color)
