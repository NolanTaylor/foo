extends Area2D

const SPRITE_LENGTH = 12
const EXTENT = 24

var direction : String = "NULL"
var hit : bool = false
var grace : float
var enemy_bodies : Array
var enemy_areas : Array

signal hit(object, dir)

func set_grace(time : float):
	grace = time
	
func set_objects(areas, bodies):
	enemy_areas = areas
	enemy_bodies = bodies
	
func _ready():
	$smacc_timer.one_shot = true
	$smacc_collisionShape.disabled = true
	$smacc_sprite.hide()

func _process(delta):
	match direction:
		"north":
			$smacc_collisionShape.position = Vector2(0, -EXTENT)
		"south":
			$smacc_collisionShape.position = Vector2(0, EXTENT)
		"east":
			$smacc_collisionShape.position = Vector2(EXTENT, 0)
		"west":
			$smacc_collisionShape.position = Vector2(-EXTENT, 0)
			
	for body in enemy_bodies:
		if overlaps_body(body) and !hit:
			hit = true
			emit_signal("hit", body, direction)
			
			if direction == "south":
				get_parent().pogo()
				
			if grace:
				get_parent().grace(grace)
				
	for area in enemy_areas:
		if overlaps_area(area) and !hit:
			hit = true
			emit_signal("hit", area, direction)
			
			if direction == "south":
				get_parent().pogo()
				
			if grace:
				get_parent().grace(grace)

func smac_smac(dir):
	hit = false
	$smacc_sprite.play("swing")
	$smacc_sprite.show()
	$smacc_collisionShape.disabled = false
	
	direction = dir

	match direction:
		"north":
			$smacc_particles.direction = Vector2(0, -1)
			$smacc_particles.position = Vector2(0, -EXTENT)
			$smacc_particles.emission_rect_extents = Vector2(32, 8)
			$smacc_sprite.position = Vector2(0, -SPRITE_LENGTH)
			$smacc_sprite.rotation_degrees = 90
			$smacc_sprite.flip_h = false
			$smacc_collisionShape.set_rotation_degrees(90)
		"south":
			$smacc_particles.direction = Vector2(0, 1)
			$smacc_particles.position = Vector2(0, EXTENT)
			$smacc_particles.emission_rect_extents = Vector2(32, 8)
			$smacc_sprite.position = Vector2(0, SPRITE_LENGTH)
			$smacc_sprite.rotation_degrees = 90
			$smacc_sprite.flip_h = true
			$smacc_collisionShape.set_rotation_degrees(90)
		"east":
			$smacc_particles.direction = Vector2(1, 0)
			$smacc_particles.position = Vector2(EXTENT, 0)
			$smacc_particles.emission_rect_extents = Vector2(8, 32)
			$smacc_sprite.position = Vector2(SPRITE_LENGTH, 0)
			$smacc_sprite.rotation_degrees = 0
			$smacc_sprite.flip_h = true
			$smacc_collisionShape.set_rotation_degrees(0)
		"west":
			$smacc_particles.direction = Vector2(-1, 0)	
			$smacc_particles.position = Vector2(-EXTENT, 0)
			$smacc_particles.emission_rect_extents = Vector2(8, 32)
			$smacc_sprite.position = Vector2(-SPRITE_LENGTH, 0)
			$smacc_sprite.rotation_degrees = 0
			$smacc_sprite.flip_h = false
			$smacc_collisionShape.set_rotation_degrees(0)

func _on_smacc_sprite_animation_finished():
	$smacc_collisionShape.disabled = true
	$smacc_sprite.stop()
	$smacc_sprite.hide()
