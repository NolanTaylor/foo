extends Area2D

const PARRY_TIME = 0.2
const PARRY_TIME_LONG = 4

var anim : String = "stay"
var active : bool = false
var shattered : bool = false # unused, for now ;)
var collision_areas : Array = []
var collision_bodies : Array = []
var shatter_areas : Array = []
var shatter_bodies : Array = []

signal parry(object, position)

func set_objects(areas, bodies, _areas = [], _bodies = []):
	collision_areas = areas
	collision_bodies = bodies
	shatter_areas = _areas
	shatter_bodies = _bodies
	
func _ready():
	hide()
	$parry_timer.one_shot = true

func _process(delta):
	$parry_sprite.play(anim)

	if active:
		for area in collision_areas:
			if overlaps_area(area):
				emit_signal("parry", area, global_position)
				
				if shatter_areas.has(collision_areas.find(area)):
					finish(true)
				else:
					finish(false)

		for body in collision_bodies:
			if overlaps_body(body):
				emit_signal("parry", body, global_position)
				
				if shatter_bodies.has(collision_bodies.find(body)):
					finish(true)
				else:
					finish(false)
					
		if $parry_timer.is_stopped():
			finish(false)
				
func summon(long = false):
	get_parent().parrying = true
	active = true
	shattered = false
	position = Vector2(0, 0)
	anim = "stay" #change to summon later
	if long:
		$parry_timer.start(PARRY_TIME_LONG)
	else:
		$parry_timer.start(PARRY_TIME)
		get_parent().parry = false
	show()

func finish(shatter = false):
	active = false
	get_parent().parrying = false
	hide()
