extends Node2D

var drawing : bool = false
var radius : float = 20.0
var pos : Vector2 = Vector2(0, 0)
var color : Color = Color("fd4d4f")

func fade(draw_pos : Vector2) -> void:
	drawing = true
	radius = 20.0
	color.a = 1.0
	pos = draw_pos

func _ready():
	pass
	
func _process(delta):
	if drawing:
		update()
		radius += 80.0 * delta
		color.a -= 2.0 * delta
		
		if radius >= 160.0:
			drawing = false
	
func _draw():
	if drawing:
		draw_arc(pos, radius, 0.0, 2.0 * PI, 200, color, 2.0, true)
		# draw_circle(pos, radius, color)
