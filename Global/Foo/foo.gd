extends KinematicBody2D

const UP : Vector2 = Vector2(0, -1)
const DOWN : Vector2 = Vector2(0, 1)
const GRAVITY : int = 30
const ACCELERATION : int = 30
const FRICTION : float = 0.2
const WALKING_FRICTION = 0.4
const AIR_FRICTION = 0.1
const DASH_FRICTION = 0.05
const MAX_SPEED = 300
const MAX_FALL_SPEED = 750
const DASH_SPEED = 1200
const DASH_TIME = 0.45
const WAVE_SPEED = 1200
const WAVE_TIME = 0.15
const POGO_SPEED = 1200
const POGO_TIME = 0.15
const INVINCIBILITY_TIME = 0.4
const COYOTE_FRAMES = 6
const JUMP_HEIGHT = 450
const POGO_HEIGHT = 750
const SMACC_COOLDOWN = 0.4
const PARRY_SHATTER_COOLDOWN = 4
const priority_animations = ["dash_out", "attack", "knees"]

var MAX_WALK_SPEED : int = 100

var combat : bool = false
var friction : bool = true
var gravity : bool = true
var control : bool = true
var limited : bool = false
var anim_override : bool = false
var dashing : bool = false
var dash : bool = true
var parry : bool = true
var wave : bool = false
var pogo : bool = false
var parrying : bool = false
var plunged : bool = false
var invincible : bool = false
var overlapped : bool = false
var talking : bool = false
var dead : bool = false
var in_air : bool = false
var coyote : int = 0
var velocity : Vector2 = Vector2(0, 0)
var anim : String = "NULL"
var direction : String = "NULL"
var pull_direction : String = "NULL"
var enemy_collision_id : Array = []

signal interact
signal advance_text
signal bounce(collider, position)
signal dash
signal move
signal dead

func set_id(id):
	enemy_collision_id = id
	
func plunger_collide(plunge_direction):
	plunged = true
	pull_direction = plunge_direction
	
func overlap():
	overlapped = true

func death():
	if !invincible and !dead:
		$smacc.hide()
		hide()
		get_parent().get_node("death_particles").burst( \
		global_position + Vector2(0, -15))
		velocity = Vector2(0, 0)
		gravity = false
		control = false
		dead = true
		$glass.play()
		$foo_collisionShape_ball.disabled = true
		$death_detector/collisionShape_ball.disabled = true
		$foo_collisionShape_stand.disabled = true
		$death_detector/collisionShape_stand.disabled = true
		emit_signal("dead")
		
func alive(pos = Vector2(50, 50)):
	global_position = pos
	velocity = Vector2(0, 0)
	dead = false
	control = true
	gravity = true
	invincible = true
	anim = "jump"
	$foo_sprite.frame = 0
	$invincibility_timer.start(INVINCIBILITY_TIME)
	$foo_collisionShape_stand.disabled = false
	$death_detector/collisionShape_stand.disabled = false
	$foo_collisionShape_ball.disabled = true
	$death_detector/collisionShape_ball.disabled = true
	$smacc.show()
	show()
	
func grace(time : float = INVINCIBILITY_TIME):
	invincible = true
	$invincibility_timer.start(time)
	
func bounce(v):
	velocity = v
	anim = "jump"
	$foo_sprite.frame = 0
	$foo_collisionShape_stand.disabled = false
	$death_detector/collisionShape_stand.disabled = false
	$foo_collisionShape_ball.disabled = true
	$death_detector/collisionShape_ball.disabled = true
	dash = true
	
func crystal():
	$parry.summon(true)
	
func shook():
	if is_on_floor() and !dashing:
		death()
	
func restore():
	parry = false
	parrying = true
	$parry.position = Vector2(0, 0)
	$parry.summon(true)
	
func pogo():
	anim = "jump"
	dash = true
	velocity.y = -POGO_HEIGHT
	# $idle_particles.hide()
	
func talk():
	velocity = Vector2(0, 0)
	control = false
	talking = true
	
func _ready():
	$dash_timer.one_shot = true
	$wave_timer.one_shot = true
	$pogo_timer.one_shot = true
	$invincibility_timer.one_shot = true
	$parry.hide()
	
func _physics_process(delta):
	friction = false
	
	# temporary, for playtesters
#	if Input.is_key_pressed(KEY_P):
#		if Input.is_action_just_pressed("ui_smacc"):
#			global_position = get_global_mouse_position()
	# --------------------
	
	if control:
		run_inputs()
	elif limited:
		run_limited_inputs()
	elif talking:
		run_talking()
		
	if dashing:
		run_dashing(delta)
		
	if plunged:
		run_plunging()
		
	if invincible and $invincibility_timer.is_stopped():
		invincible = false
		
	if wave and $wave_timer.is_stopped():
		wave = false
		
	if pogo and $pogo_timer.is_stopped():
		pogo = false
	
	if gravity:
		if anim == "dash_in":
			velocity.y += GRAVITY / 3
		else:
			velocity.y = min(velocity.y + GRAVITY, MAX_FALL_SPEED)
		
	if is_on_floor():
		coyote = COYOTE_FRAMES
		parry = true
		
		if in_air:
			$land.play()
			in_air = false
			if !dash:
				$trail.restart()
		
		if friction:
			if combat:
				if velocity.x > MAX_SPEED:
					velocity.x -= ACCELERATION
				elif velocity.x < -MAX_SPEED:
					velocity.x += ACCELERATION
				else:
					velocity.x = lerp(velocity.x, 0, FRICTION)
			else:
				velocity.x = lerp(velocity.x, 0, WALKING_FRICTION)
		if !dashing:
			dash = true
			$trail.emitting = true
	else:
		if coyote > 0:
			coyote -= 1
		if friction:
			if velocity.x > MAX_SPEED:
					velocity.x -= ACCELERATION
			elif velocity.x < -MAX_SPEED:
				velocity.x += ACCELERATION
			else:
				velocity.x = lerp(velocity.x, 0, AIR_FRICTION)
		if gravity and velocity.y > GRAVITY and anim != "fall" and \
		anim != "dash_out" and anim != "dash_in" and anim != "knees":
				anim = "jump"
		in_air = true
	
	if anim != "NULL" and !anim_override:
		$foo_sprite.play(anim)
		
#	if !dashing:
#		move_and_collide(velocity * delta)
	
	if !dashing:
		velocity = move_and_slide(velocity, UP)

		for i in get_slide_count():
			var collision = get_slide_collision(i)

			for id in enemy_collision_id:
				if collision.collider_id == id:
					death()

func run_inputs():
	if Input.is_action_just_pressed("ui_dash") and \
	(dash or get_node("/root/Config").infinite_dash) and combat:
		direction = "NULL"
		
		if Input.is_action_pressed("ui_right"):
			if Input.is_action_pressed("ui_up"):
				direction = "north_east"
			elif Input.is_action_pressed("ui_down"):
				direction = "south_east"
			else:
				direction = "east"
		elif Input.is_action_pressed("ui_left"):
			if Input.is_action_pressed("ui_up"):
				direction = "north_west"
			elif Input.is_action_pressed("ui_down"):
				direction = "south_west"
			else:
				direction = "west"
		elif Input.is_action_pressed("ui_up"):
			direction = "north"
		elif Input.is_action_pressed("ui_down"):
			direction = "south"
		else:
			if $foo_sprite.flip_h:
				direction = "east"
			elif !$foo_sprite.flip_h:
				direction = "west"
		
		control = false
		limited = true
		gravity = true
		friction = false
		dashing = true
		dash = false
		anim = "dash_in"
		velocity.x /= 2
		velocity.y /= 2
		$foo_collisionShape_stand.disabled = true
		$death_detector/collisionShape_stand.disabled = true
		$foo_collisionShape_ball.disabled = false
		$death_detector/collisionShape_ball.disabled = false
		$dash_timer.start(DASH_TIME)
	elif Input.is_action_just_pressed("ui_parry") and \
	parry and !parrying and !dashing and combat:
		$parry.summon()
	elif Input.is_action_just_pressed("ui_jump") and combat:
		if is_on_floor() or coyote:
			velocity.y = -JUMP_HEIGHT
			
			if anim in priority_animations:
				pass
			else:
				anim = "jump"
		elif is_on_wall():
			pass
	elif Input.is_action_just_pressed("ui_smacc") and \
	$smacc/smacc_timer.is_stopped() and combat:
		$smacc/smacc_particles.emitting = true
		$smacc/smacc_timer.start(SMACC_COOLDOWN)
		anim = "attack"
		
		if Input.is_action_pressed("ui_up"):
			$smacc.smac_smac("north")
		elif Input.is_action_pressed("ui_down") and !is_on_floor():
			$smacc.smac_smac("south")
		elif Input.is_action_pressed("ui_left"):
			$smacc.smac_smac("west")
		elif Input.is_action_pressed("ui_right"):
			$smacc.smac_smac("east")
		else: 
			if $foo_sprite.flip_h:
				$smacc.smac_smac("east")
			else:
				$smacc.smac_smac("west")
	elif Input.is_action_just_pressed("ui_interact") and !combat:
		if overlapped:
			emit_signal("interact")
	elif Input.is_action_just_pressed("restart"):
		death()
	elif Input.is_action_pressed("ui_left"):
		emit_signal("move")
		if combat:
			if velocity.x > -MAX_SPEED:
				velocity.x -= ACCELERATION
			
			if abs(velocity.x) > MAX_SPEED:
				friction = true
		else:
			velocity.x = max(velocity.x - ACCELERATION, \
			-MAX_WALK_SPEED)
			
		$foo_sprite.flip_h = false
	
		if is_on_floor():
			if anim in priority_animations:
				pass
			else:
				if combat:
					anim = "run"
				else:
					anim = "walk"
					
	elif Input.is_action_pressed("ui_right"):
		emit_signal("move")
		if combat:
			if velocity.x < MAX_SPEED:
				velocity.x += ACCELERATION
			
			if abs(velocity.x) > MAX_SPEED:
				friction = true
		else:
			velocity.x = min(velocity.x + ACCELERATION, MAX_WALK_SPEED)
			
		$foo_sprite.flip_h = true
	
		if is_on_floor():
			if anim in priority_animations:
				pass
			else:
				if combat:
					anim = "run"
				else:
					anim = "walk"
	else:
		friction = true
	
		if is_on_floor():
			if anim in priority_animations:
				pass
			else:
				anim = "idle" # idle combat doesn't look good :(
					
func run_limited_inputs():
	if Input.is_action_just_pressed("ui_parry"):
		wave = true
		$wave_timer.start(WAVE_TIME)
		
	if Input.is_action_just_pressed("ui_smacc"):
		pogo = true
		$pogo_timer.start(POGO_TIME)
					
func run_talking():
	if Input.is_action_just_pressed("ui_jump") or \
	Input.is_action_just_pressed("ui_interact"):
		emit_signal("advance_text")
			
func run_dashing(delta):
	if anim != "dash_in":
		velocity.x = lerp(velocity.x, 0, DASH_FRICTION)
		velocity.y = lerp(velocity.y, 0, DASH_FRICTION)
			
	var collision = move_and_collide(velocity * delta)
	
	if collision:
		for id in enemy_collision_id:
			if collision.collider_id == id:
				death()
			
		if anim != "dash_in":
			$bounce.play()
			emit_signal("bounce", collision.collider, \
			collision.position)
				
		velocity = velocity.bounce(collision.normal)
		dash = true
		
		if wave and pogo:
			$dash_timer.stop()
			velocity = Vector2(0, 0)
		elif wave:
			wave = false
			
			if int(collision.normal.x) == 0:
				if velocity.x > 0:
					velocity.x += WAVE_SPEED
				elif velocity.x < 0:
					velocity.x -= WAVE_SPEED
					
				if velocity.y > 0:
					velocity.y -= WAVE_SPEED / 4
				elif velocity.x < 0:
					velocity.y += WAVE_SPEED / 4
			else:
				if velocity.y > 0:
					velocity.y += WAVE_SPEED / 2
					
					if velocity.x > 0:
						velocity.x -= WAVE_SPEED / 4
					else:
						velocity.x += WAVE_SPEED / 4
				elif velocity.y < 0:
					velocity.y -= WAVE_SPEED / 2
					
					if velocity.x > 0:
						velocity.x -= WAVE_SPEED / 4
					else:
						velocity.x += WAVE_SPEED / 4	
		elif pogo:
			pogo = false

			match (Vector2(int(collision.normal.x), \
			int(collision.normal.y))):
				Vector2(1, 0):
					$smacc.smac_smac("west")
					velocity.x = POGO_SPEED
				Vector2(-1, 0):
					$smacc.smac_smac("east")
					velocity.x = -POGO_SPEED
				Vector2(0, 1):
					$smacc.smac_smac("north")
					velocity.y = POGO_SPEED
				Vector2(0, -1):
					$smacc.smac_smac("south")
					velocity.y = -POGO_SPEED

	if $dash_timer.is_stopped():
		dashing = false
		gravity = true
		control = true
		limited = false
		move_and_slide(Vector2(0, 0))
		anim = "dash_out"
		
		$rayCast.enabled = true
		$rayCast.force_raycast_update()
		if $rayCast.get_collider():
			position.y -= 24
		$rayCast.enabled = false
		
func run_plunging():
	control = false
	plunged = true
	anim = "idle"
	
	if pull_direction == "left":
		velocity.x = 100
	elif pull_direction == "right":
		velocity.x = -100
		
func set_direction():
	if Input.is_action_pressed("ui_right"):
		if Input.is_action_pressed("ui_up"):
			direction = "north_east"
		elif Input.is_action_pressed("ui_down"):
			direction = "south_east"
		else:
			if direction != "north_east" and direction != "south_east":
				direction = "east"
	elif Input.is_action_pressed("ui_left"):
		if Input.is_action_pressed("ui_up"):
			direction = "north_west"
		elif Input.is_action_pressed("ui_down"):
			direction = "south_west"
		else:
			if direction != "north_west" and direction != "south_west":
				direction = "west"
	elif Input.is_action_pressed("ui_up"):
		if Input.is_action_pressed("ui_right"):
			direction = "north_east"
		elif Input.is_action_pressed("ui_left"):
			direction = "north_west"
		else:
			if direction != "north_east" and direction != "north_west":
				direction = "north"
	elif Input.is_action_pressed("ui_down"):
		if Input.is_action_pressed("ui_right"):
			direction = "south_east"
		elif Input.is_action_pressed("ui_left"):
			direction = "south_west"
		else:
			if direction != "south_east" and direction != "south_west":
				direction = "south"
	else:
		pass
	
	match direction:
		"north_west":
			velocity.x = -DASH_SPEED / 1.5
			velocity.y = -DASH_SPEED / 1.5
		"north_east":
			velocity.x = DASH_SPEED / 1.5
			velocity.y = -DASH_SPEED / 1.5
		"south_west":
			velocity.x = -DASH_SPEED / 1.5
			velocity.y = DASH_SPEED / 1.5
		"south_east":
			velocity.x = DASH_SPEED / 1.5
			velocity.y = DASH_SPEED / 1.5
		"west":
			velocity.x = -DASH_SPEED
			velocity.y = 0
		"east":
			velocity.x = DASH_SPEED
			velocity.y = 0
		"north":
			velocity.x = 0
			velocity.y = -DASH_SPEED
		"south":
			velocity.x = 0
			velocity.y = DASH_SPEED

func _on_player_sprite_animation_finished():
	$foo_sprite.stop()
	
	match anim:
		"dash_in":
			anim = "dash"
			gravity = false
			set_direction()
			emit_signal("dash")
			$foo_sprite.animation = "dash"
		"dash_out":
			anim = "jump"
			if !dash:
				$"../dash_circle".fade(position)
				$trail.emitting = false
			$foo_collisionShape_ball.disabled = true
			$death_detector/collisionShape_ball.disabled = true
			$foo_collisionShape_stand.disabled = false
			$death_detector/collisionShape_stand.disabled = false
		"attack":
			anim = "idle_combat"
		"jump":
			$foo_sprite.play("fall")
			anim = "fall"
		"door":
			anim = "door_hold"
		"stand_up":
			anim = "idle"
			combat = false
			control = true
