extends Control

func _ready():
	pass

func _process(delta):
	if Input.is_action_pressed("ui_down"):
		$VBoxContainer2/ScrollContainer.scroll_vertical += 16
	elif Input.is_action_pressed("ui_up"):
		$VBoxContainer2/ScrollContainer.scroll_vertical -= 16
