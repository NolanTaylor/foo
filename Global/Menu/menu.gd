extends Control

const TRANS_SPEED : int = 60
const BLACK : Color = Color(0, 0, 0)
const WHITE : Color = Color(1.0, 1.0, 1.0)

var going_to_play : bool = false
var transitioning : bool = false
var current_level : String = \
"res://Platforming/Level_1/level_1_lock.tscn"
var changing_input : String = "null"
var last_input : InputEventKey
var moving_sprite_out : Sprite
var moving_sprite_in : Sprite

var menu_dark = preload( \
"res://Assets/menu.png")
var menu_wallpaper_dark = preload( \
"res://Assets/menu_wallpaper.png")
var control_center_dark = preload( \
"res://Assets/control_center.png")
var control_center_wallpaper_dark = preload( \
"res://Assets/control_center_wallpaper.png")
var display_brightness_dark = preload( \
"res://Assets/display_brightness.png")
var display_brightness_wallpaper_dark = preload( \
"res://Assets/display_brightness_wallpaper.png")
var sounds_haptics_dark = preload( \
"res://Assets/sounds_haptics.png")
var sounds_haptics_wallpaper_dark = preload( \
"res://Assets/sounds_haptics_wallpaper.png")
var accessibility_dark = preload( \
"res://Assets/accessibility.png")
var accessibility_wallpaper_dark = preload( \
"res://Assets/accessibility_wallpaper.png")
var check_off_dark = preload( \
"res://Assets/check_off.png")
var check_on_dark = preload( \
"res://Assets/check_on.png")

var menu_light = preload( \
"res://Assets/menu_light.png")
var menu_wallpaper_light = preload( \
"res://Assets/menu_wallpaper_light.png")
var control_center_light = preload( \
"res://Assets/control_center_light.png")
var control_center_wallpaper_light = preload( \
"res://Assets/control_center_wallpaper_light.png")
var display_brightness_light = preload( \
"res://Assets/display_brightness_light.png")
var display_brightness_wallpaper_light = preload( \
"res://Assets/display_brightness_wallpaper_light.png")
var sounds_haptics_light = preload( \
"res://Assets/sounds_haptics_light.png")
#var sounds_haptics_wallpaper_light = preload( \
#"res://Assets/sounds_haptics_wallpaper_light.png")
var accessibility_light = preload( \
"res://Assets/accessibility_light.png")
var accessibility_wallpaper_light = preload( \
"res://Assets/accessibility_wallpaper_light.png")
var check_off_light = preload( \
"res://Assets/check_off_light.png")
var check_on_light = preload( \
"res://Assets/check_on_light.png")

func _ready():
	$timer.one_shot = true
	
	$music.play()
	
	current_level = get_node("/root/Config").level
	
	if get_node("/root/Config").paused:
		$menu/play_label.text = "Resume"
	
	if get_node("/root/Config").left:
		InputMap.action_erase_events("ui_left")
		InputMap.action_add_event("ui_left", \
			get_node("/root/Config").left)
		$control_center/change_left.text = \
			get_node("/root/Config").left.as_text()
	if get_node("/root/Config").right:
		InputMap.action_erase_events("ui_right")
		InputMap.action_add_event("ui_right", \
			get_node("/root/Config").right)
		$control_center/change_right.text = \
			get_node("/root/Config").right.as_text()
	if get_node("/root/Config").up:
		InputMap.action_erase_events("ui_up")
		InputMap.action_add_event("ui_up", \
			get_node("/root/Config").up)
		$control_center/change_up.text = \
			get_node("/root/Config").up.as_text()
	if get_node("/root/Config").down:
		InputMap.action_erase_events("ui_down")
		InputMap.action_add_event("ui_down", \
			get_node("/root/Config").down)
		$control_center/change_down.text = \
			get_node("/root/Config").down.as_text()
	if get_node("/root/Config").jump:
		InputMap.action_erase_events("ui_jump")
		InputMap.action_erase_events("ui_accept")
		InputMap.action_add_event("ui_jump", \
			get_node("/root/Config").jump)
		InputMap.action_add_event("ui_accept", \
			get_node("/root/Config").jump)
		$control_center/change_jump.text = \
			get_node("/root/Config").jump.as_text()
	if get_node("/root/Config").dash:
		InputMap.action_erase_events("ui_dash")
		InputMap.action_add_event("ui_dash", \
			get_node("/root/Config").dash)
		$control_center/change_dash.text = \
			get_node("/root/Config").dash.as_text()
	if get_node("/root/Config").restart:
		InputMap.action_erase_events("restart")
		InputMap.action_add_event("restart", \
			get_node("/root/Config").restart)
		$control_center/change_down.text = \
			get_node("/root/Config").restart.as_text()
	if get_node("/root/Config").pause:
		InputMap.action_erase_events("pause")
		InputMap.action_add_event("pause", \
			get_node("/root/Config").pause)
		$control_center/change_pause.text = \
			get_node("/root/Config").pause.as_text()
	if get_node("/root/Config").cam_left:
		InputMap.action_erase_events("cam_left")
		InputMap.action_add_event("cam_left", \
			get_node("/root/Config").cam_left)
		$control_center/change_cam_left.text = \
			get_node("/root/Config").cam_left.as_text()
	if get_node("/root/Config").cam_right:
		InputMap.action_erase_events("cam_right")
		InputMap.action_add_event("cam_right", \
			get_node("/root/Config").cam_right)
		$control_center/change_cam_right.text = \
			get_node("/root/Config").cam_right.as_text()
	if get_node("/root/Config").cam_up:
		InputMap.action_erase_events("cam_up")
		InputMap.action_add_event("cam_up", \
			get_node("/root/Config").cam_up)
		$control_center/change_cam_up.text = \
			get_node("/root/Config").cam_up.as_text()
	if get_node("/root/Config").cam_down:
		InputMap.action_erase_events("cam_down")
		InputMap.action_add_event("cam_down", \
			get_node("/root/Config").cam_down)
		$control_center/change_cam_down.text = \
			get_node("/root/Config").cam_down.as_text()
			
	if get_node("/root/Config").light_mode:
		switch_to_light_mode()
	if get_node("/root/Config").fullscreen:
		OS.window_fullscreen = true
		$display_brightness/fullscreen.pressed = true
		
	$sounds_haptics/master_slider.value = \
		get_node("/root/Config").master_volume
	AudioServer.set_bus_volume_db( \
		AudioServer.get_bus_index("Master"), linear2db( \
			$sounds_haptics/master_slider.value))
	$sounds_haptics/music_slider.value = \
		get_node("/root/Config").music_volume
	AudioServer.set_bus_volume_db( \
		AudioServer.get_bus_index("Music"), linear2db( \
			$sounds_haptics/music_slider.value))
	$sounds_haptics/soundfx_slider.value = \
		get_node("/root/Config").sound_fx_volume
	AudioServer.set_bus_volume_db( \
		AudioServer.get_bus_index("Sound_FX"), linear2db( \
			$sounds_haptics/soundfx_slider.value))
		
	$accessibility/invincibility_checkButton.pressed = \
		get_node("/root/Config").invincibility
	$accessibility/infinite_dash_checkButton.pressed = \
		get_node("/root/Config").infinite_dash
		
	$menu/play.grab_focus()
	
func _process(delta):
	if going_to_play and $timer.is_stopped():
		get_node("/root/Config").save()
		get_tree().change_scene(current_level)
	if transitioning:
		if moving_sprite_in == $menu:
			moving_sprite_in.position.x += TRANS_SPEED
			moving_sprite_out.position.x += TRANS_SPEED
			
			if moving_sprite_in.position.x >= \
			moving_sprite_in.get_node("rest").position.x:
				transitioning = false
				moving_sprite_in.position.x = \
				moving_sprite_in.get_node("rest").position.x
				moving_sprite_out.hide()
				
				for child in moving_sprite_out.get_children():
					if child == Button:
						child.disabled = true
				for child in moving_sprite_in.get_children():
					if child == Button:
						child.disabled = false
						
			$menu/play.grab_focus()
		else:
			moving_sprite_in.position.x -= TRANS_SPEED
			moving_sprite_out.position.x -= TRANS_SPEED
			
			if moving_sprite_in.position.x <= \
			moving_sprite_in.get_node("rest").position.x:
				transitioning = false
				moving_sprite_in.position.x = \
				moving_sprite_in.get_node("rest").position.x
				moving_sprite_out.hide()
				
				for child in moving_sprite_out.get_children():
					if child == Button:
						child.disabled = true
				for child in moving_sprite_in.get_children():
					if child == Button:
						child.disabled = false
						
func _input(event):
	if event.is_pressed():
		match changing_input:
			"null":
				pass
			"left":
				InputMap.action_erase_events("ui_left")
				InputMap.action_add_event("ui_left", event)
				$control_center/change_left.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").left = event
				changing_input = "null"
			"right":
				InputMap.action_erase_events("ui_right")
				InputMap.action_add_event("ui_right", event)
				$control_center/change_right.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").right = event
				changing_input = "null"
			"up":
				InputMap.action_erase_events("ui_up")
				InputMap.action_add_event("ui_up", event)
				$control_center/change_up.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").up = event
				changing_input = "null"
			"down":
				InputMap.action_erase_events("ui_down")
				InputMap.action_add_event("ui_down", event)
				$control_center/change_down.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").down = event
				changing_input = "null"
			"jump":
				InputMap.action_erase_events("ui_jump")
				InputMap.action_erase_events("ui_accept")
				InputMap.action_add_event("ui_jump", event)
				InputMap.action_add_event("ui_accept", event)
				$control_center/change_jump.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").jump = event
				changing_input = "null"
			"dash":
				InputMap.action_erase_events("ui_dash")
				InputMap.action_add_event("ui_dash", event)
				$control_center/change_dash.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").dash = event
				changing_input = "null"
			"restart":
				InputMap.action_erase_events("restart")
				InputMap.action_add_event("restart", event)
				$control_center/change_restart.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").restart = event
				changing_input = "null"
			"pause":
				InputMap.action_erase_events("pause")
				InputMap.action_add_event("pause", event)
				$control_center/change_pause.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").pause = event
				changing_input = "null"
			"cam_left":
				InputMap.action_erase_events("cam_left")
				InputMap.action_add_event("cam_left", event)
				$control_center/change_cam_left.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").cam_left = event
				changing_input = "null"
			"cam_right":
				InputMap.action_erase_events("cam_left")
				InputMap.action_add_event("cam_left", event)
				$control_center/change_cam_right.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").cam_right = event
				changing_input = "null"
			"cam_up":
				InputMap.action_erase_events("cam_up")
				InputMap.action_add_event("cam_up", event)
				$control_center/change_cam_up.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").cam_up = event
				changing_input = "null"
			"cam_down":
				InputMap.action_erase_events("cam_down")
				InputMap.action_add_event("cam_down", event)
				$control_center/change_cam_down.text = event.as_text()
				$control_center/popup.hide()
				get_node("/root/Config").cam_down = event
				changing_input = "null"
				
	if event is InputEventJoypadButton or \
		event is InputEventJoypadMotion:
			Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	elif event is InputEventMouseMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
				
func switch_to_light_mode():
	$menu.texture = menu_light
	$menu_wallpaper.texture = menu_wallpaper_light
	for child in $menu.get_children():
		if child is Label:
			child.set("custom_colors/font_color", BLACK)
	
	$control_center.texture = control_center_light
	$control_center_wallpaper.texture = control_center_wallpaper_light
	for child in $control_center.get_children():
		if child is Label and child.name != "settings":
			child.set("custom_colors/font_color", BLACK)
		if child is Button:
			child.set("custom_colors/font_color", BLACK)
			child.set("custom_colors/font_color_hover", BLACK)
			child.set("custom_colors/font_color_pressed", BLACK)
		
	$display_brightness.texture = display_brightness_light
	$display_brightness_wallpaper.texture = \
		display_brightness_wallpaper_light
	$display_brightness/fullscreen.set( \
		"custom_icons/off", check_off_light)
	$display_brightness/fullscreen.set( \
		"custom_icons/on", check_on_light)
	$display_brightness/display_brightness.set( \
		"custom_colors/font_color", BLACK)
	$display_brightness/fullscreen_label.set( \
		"custom_colors/font_color", BLACK)
		
	$sounds_haptics.texture = sounds_haptics_light
#	$sounds_haptics_wallpaper.texture = sounds_haptics_wallpaper_light
#	$sounds_haptics/master_slider.set( \
#		"custom_styles/slider", stylebox_light)
#	$sounds_haptics/music_slider.set( \
#		"custom_styles/slider", stylebox_light)
#	$sounds_haptics/soundfx_slider.set( \
#		"custom_styles/slider", stylebox_light)
	$sounds_haptics/sounds_haptics.set( \
		"custom_colors/font_color", BLACK)
		
	$accessibility.texture = accessibility_light
	$accessibility_wallpaper.texture = accessibility_wallpaper_light
	$accessibility/accessibility.set( \
		"custom_colors/font_color", BLACK)
	$accessibility/invincibility.set( \
		"custom_colors/font_color", BLACK)
	$accessibility/infinite_dash.set( \
		"custom_colors/font_color", BLACK)
	$accessibility/invincibility_checkButton.set( \
		"custom_icons/off", check_off_light)
	$accessibility/invincibility_checkButton.set( \
		"custom_icons/on", check_on_light)
	$accessibility/infinite_dash_checkButton.set( \
		"custom_icons/off", check_off_light)
	$accessibility/infinite_dash_checkButton.set( \
		"custom_icons/on", check_on_light)
	$accessibility/accessibility.set( \
		"custom_colors/font_color", BLACK)
		
func switch_to_dark_mode():
	$menu.texture = menu_dark
	$menu_wallpaper.texture = menu_wallpaper_dark
	for child in $menu.get_children():
		if child is Label:
			child.set("custom_colors/font_color", WHITE)
			
	$control_center.texture = control_center_dark
	$control_center_wallpaper.texture = control_center_wallpaper_dark
	for child in $control_center.get_children():
		if child is Label and child.name != "settings":
			child.set("custom_colors/font_color", WHITE)
		if child is Button:
			child.set("custom_colors/font_color", WHITE)
			child.set("custom_colors/font_color_hover", WHITE)
			child.set("custom_colors/font_color_pressed", WHITE)
	
	$display_brightness.texture = display_brightness_dark
	$display_brightness_wallpaper.texture = \
		display_brightness_wallpaper_dark
	$display_brightness/fullscreen.set( \
		"custom_icons/off", check_off_dark)
	$display_brightness/fullscreen.set( \
		"custom_icons/on", check_on_dark)
	$display_brightness/display_brightness.set( \
		"custom_colors/font_color", WHITE)
	$display_brightness/fullscreen_label.set( \
		"custom_colors/font_color", WHITE)
		
	$sounds_haptics.texture = sounds_haptics_dark
#	$sounds_haptics_wallpaper.texture = sounds_haptics_wallpaper_light
#	$sounds_haptics/master_slider.set( \
#		"custom_styles/slider", stylebox_dark)
#	$sounds_haptics/music_slider.set( \
#		"custom_styles/slider", stylebox_dark)
#	$sounds_haptics/soundfx_slider.set( \
#		"custom_styles/slider", stylebox_dark)
#	$sounds_haptics/sounds_haptics.set( \
#		"custom_colors/font_color", WHITE)
		
	$accessibility.texture = accessibility_dark
	$accessibility_wallpaper.texture = accessibility_wallpaper_dark
	$accessibility/accessibility.set( \
		"custom_colors/font_color", WHITE)
	$accessibility/invincibility.set( \
		"custom_colors/font_color", WHITE)
	$accessibility/infinite_dash.set( \
		"custom_colors/font_color", WHITE)
	$accessibility/invincibility_checkButton.set( \
		"custom_icons/off", check_off_dark)
	$accessibility/invincibility_checkButton.set( \
		"custom_icons/on", check_on_dark)
	$accessibility/infinite_dash_checkButton.set( \
		"custom_icons/off", check_off_dark)
	$accessibility/infinite_dash_checkButton.set( \
		"custom_icons/on", check_on_dark)
	$accessibility/accessibility.set( \
		"custom_colors/font_color", WHITE)
	
func _on_play_pressed():
	$timer.start(2.5)
	$music.stop()
	$lock.play()
	$canvasLayer/colorRect.color.a = 1.0
	$menu/play.disabled = true
	$menu/control_center.disabled = true
	$menu/display_brightness.disabled = true
	$menu/sounds_haptics.disabled = true
	$menu/accessibility.disabled = true
	$menu/quit.disabled = true
	going_to_play = true
	
func _on_control_center_pressed():
	transitioning = true
	moving_sprite_in = $control_center
	moving_sprite_out = $menu
	$menu_wallpaper.hide()
	$control_center_wallpaper.show()
	$control_center.show()
	$control_center.position.x = 1024
	$control_center/change_left.grab_focus()
	
func _on_display_brightness_pressed():
	transitioning = true
	moving_sprite_in = $display_brightness
	moving_sprite_out = $menu
	$menu_wallpaper.hide()
	$display_brightness_wallpaper.show()
	$display_brightness.show()
	$display_brightness.position.x = 1024
	$display_brightness/light.grab_focus()
	
func _on_sounds_haptics_pressed():
	transitioning = true
	moving_sprite_in = $sounds_haptics
	moving_sprite_out = $menu
	$menu_wallpaper.hide()
	$sounds_haptics_wallpaper.show()
	$sounds_haptics.show()
	$sounds_haptics.position.x = 1024
	$sounds_haptics/master_slider.grab_focus()
	
func _on_accessibility_pressed():
	transitioning = true
	moving_sprite_in = $accessibility
	moving_sprite_out = $menu
	$menu_wallpaper.hide()
	$accessibility_wallpaper.show()
	$accessibility.show()
	$accessibility.position.x = 1024
	$accessibility/invincibility_checkButton.grab_focus()
	
func _on_reset_pressed():
	$confirm_reset.popup(Rect2(400, 200, 200, 60))
	
func _on_quit_pressed():
	get_node("/root/Config").save()
	get_tree().quit()
	
func _on_from_control_center_pressed():
	transitioning = true
	changing_input = "null"
	moving_sprite_in = $menu
	moving_sprite_out = $control_center
	$control_center_wallpaper.hide()
	$menu_wallpaper.show()
	$menu.show()
	$menu.position.x = -400
	
func _on_from_display_pressed():
	transitioning = true
	moving_sprite_in = $menu
	moving_sprite_out = $display_brightness
	$display_brightness_wallpaper.hide()
	$menu_wallpaper.show()
	$menu.show()
	$menu.position.x = -400
	
func _on_from_sounds_haptics_pressed():
	transitioning = true
	moving_sprite_in = $menu
	moving_sprite_out = $sounds_haptics
	$sounds_haptics_wallpaper.hide()
	$menu_wallpaper.show()
	$menu.show()
	$menu.position.x = -400
	
func _on_from_accessibility_pressed():
	transitioning = true
	moving_sprite_in = $menu
	moving_sprite_out = $accessibility
	$accessibility_wallpaper.hide()
	$menu_wallpaper.show()
	$menu.show()
	$menu.position.x = -400
	
func _on_change_left_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Left Input"
	changing_input = "left"
	
func _on_change_right_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Right Input"
	changing_input = "right"
	
func _on_change_up_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Up Input"
	changing_input = "up"
	
func _on_change_down_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Down Input"
	changing_input = "down"
	
func _on_change_jump_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Jump Input"
	changing_input = "jump"
	
func _on_change_dash_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Spin Input"
	changing_input = "dash"
	
func _on_change_restart_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Restart Input"
	changing_input = "restart"
	
func _on_change_pause_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Pause Input"
	changing_input = "pause"
	
func _on_change_cam_left_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Camera Left Input"
	changing_input = "cam_left"
	
func _on_change_cam_right_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Camera Right Input"
	changing_input = "cam_right"
	
func _on_change_cam_up_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Camera Up Input"
	changing_input = "cam_up"
	
func _on_change_cam_down_pressed():
	$control_center/popup.popup(Rect2(352, 96, 320, 160))
	$control_center/popup.window_title = "Change Camera Down Input"
	changing_input = "cam_down"
	
func _on_light_pressed():
	if !get_node("/root/Config").light_mode:
		get_node("/root/Config").light_mode = true
		switch_to_light_mode()
	
func _on_dark_pressed():
	if get_node("/root/Config").light_mode:
		get_node("/root/Config").light_mode = false
		switch_to_dark_mode()
	
func _on_fullscreen_toggled(button_pressed):
	if button_pressed:
		OS.window_fullscreen = true
		get_node("/root/Config").fullscreen = true
	else:
		OS.window_fullscreen = false
		get_node("/root/Config").fullscreen = false
	
func _on_master_slider_value_changed(value):
	get_node("/root/Config").master_volume = value
	AudioServer.set_bus_volume_db( \
	AudioServer.get_bus_index("Master"), linear2db(value))

func _on_music_slider_value_changed(value):
	get_node("/root/Config").music_volume = value
	AudioServer.set_bus_volume_db( \
	AudioServer.get_bus_index("Music"), linear2db(value))

func _on_soundfx_slider_value_changed(value):
	get_node("/root/Config").sound_fx_volume = value
	AudioServer.set_bus_volume_db( \
	AudioServer.get_bus_index("Sound_FX"), linear2db(value))

func _on_invincibility_checkButton_toggled(button_pressed):
	if button_pressed:
		get_node("/root/Config").invincibility = true
	else:
		get_node("/root/Config").invincibility = false

func _on_infinite_dash_checkButton_toggled(button_pressed):
	if button_pressed:
		get_node("/root/Config").infinite_dash = true
	else:
		get_node("/root/Config").infinite_dash = false

func _on_confirm_reset_confirmed():
	get_node("/root/Config").reset()
	current_level = "res://Platforming/Level_1/level_1_lock.tscn"
