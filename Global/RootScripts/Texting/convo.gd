extends Node2D

var act : int = 0
var player_body

func set_body(body):
	player_body = body

func _ready():
	modulate.a = 0.0
	$fade_timer.one_shot = true
	
func _process(delta):
	match act:
		0:
			if $trigger.overlaps_body(player_body):
				act += 1
				$fade_timer.start(8)
		1:
			modulate.a += 0.5 * delta
			if modulate.a > 1.0:
				modulate.a = 1.0
			if $fade_timer.is_stopped():
				act += 1
		2:
			modulate.a -= 0.5 * delta
			if modulate.a <= 0.0:
				hide()
				act += 1
		3:
			pass
