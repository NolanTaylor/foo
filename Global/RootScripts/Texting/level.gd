extends Node2D

onready var tween_out = get_node("tween")

const DEATH_TIME = 2

var transitioning : bool = false
var transition_finish : bool = false
var knees : bool = false
var dead : bool = false
var cam_index : int = 0
var point_index : int = 0
var to_level : int = 0
var direction : String
var respawn_pos : Vector2
var cam : Rect2

var convo_light

var current_checkpoint_pos : Vector2
var current_checkpoint_texts : Array
var current_checkpoint_cam : int

func _ready():
	if get_node("/root/Config").light_mode:
		VisualServer.set_default_clear_color(Color("ffffff"))
		if $camera/canvasLayer/root_convo/name.text == "4 People":
			convo_light = load("res://Assets/gc_light.png")
		else:
			convo_light = load("res://Assets/dm_light.png")
		if has_node("north"):
			var arrows_light = load("res://Assets/arrows_light.png")
			$north_east.texture = arrows_light
			$north_west_bounce.texture = arrows_light
			$north.texture = arrows_light
			$south_east_bounce.texture = arrows_light
		if has_node("south"):
			var arrows_light = load("res://Assets/arrows_light.png")
			$south.texture = arrows_light
			$east.texture = arrows_light
		$camera/canvasLayer/root_convo/convo.texture = convo_light
		$camera/canvasLayer/colorRect.color = Color("ffffff")
		$camera/canvasLayer/colorRect.color.a = 0
		for child in $camera/canvasLayer/root_convo.get_children():
			if child.name == "name":
				child.set("custom_colors/font_color", Color("000000"))
	else:
		VisualServer.set_default_clear_color(Color("000000"))
		
	if $advance.has_node("REAL_END"):
		$root_foo/foo.anim = "knees"
		$root_foo/foo.control = false
		$knees_timer.one_shot = true
		$knees_timer.start(8.0)
		
	$music.volume_db = -80
	tween_out.interpolate_property($music, "volume_db", \
		-80, -8.0, 6.0, 1, Tween.EASE_IN, 0)
	tween_out.start()
	$music.play()
	
	$camera/canvasLayer/root_convo.set_body($root_foo/foo)
	
	Input.connect("joy_connection_changed", self, \
		"_on_joy_connection_changed")
	
	$death_timer.one_shot = true
	$advance_timer.one_shot = true
	
	$camera.current = true
	
	$advance.set_body($root_foo/foo)
	$advance.connect("enter", self, "transition")
	
	$root_foo/foo.combat = true
	$root_foo/foo/foo_sprite.flip_h = true
	$root_foo/foo.connect("dead", self, "restart")
	$root_foo/foo.connect("dash", $camera, "bacc")
	$root_foo/foo.connect("move", $camera, "bacc")
	
	for child in $texts.get_children():
#		child.get_node("label").text = " " # temporary
		var text : String = child.get_node("label").text
		child.set_body($root_foo/foo)
		child.connect("death", $root_foo/foo, "death")
		child.connect("appear", self, "text_appear")
		child.sender = get_text_attr("sender", child.get_node("data"))
		child.delay = get_text_attr("delay", child.get_node("data"))
		child.flip = get_text_attr("flip", child.get_node("data"))
			
		$root_foo/foo.connect("bounce", child, "bounced")
			
		if child.has_node("translate"):
			$root_foo/foo.connect("bounce", child, "pause")
			
		if child.has_node("phase"):
			$root_foo/foo.connect("dash", child, "phase")
		
	for child in $checkpoints.get_children():
		child.set_body($root_foo/foo)
		child.connect("check", self, "checkpoint_reached")
		
	for child in $cam_changes.get_children():
		child.set_body($root_foo/foo)
		child.connect("change", self, "cam_changed")
		
#
#	bubbles kinda janky rn, don't think i want them
#
#	if has_node("bubbles"):
#		$root_foo/foo/parry.set_objects([], $bubbles.get_children())
#
#		for child in $bubbles.get_children():
#			child.set_body($root_foo/foo)
#			child.connect("bounce", $root_foo/foo, "bounce")
#			child.connect("collision", self, "bubble_collision")
#			$root_foo/foo/parry.connect("parry", child, "parried")
#
#	same w/ crystals
#
#	if has_node("crystals"):
#		for child in $crystals.get_children():
#			child.set_body($root_foo/foo)
#			child.connect("collect", $root_foo/foo, "crystal")
			
	$cam_changes/cam0.override_signal()
	
	if get_node("/root/Config").paused:
		get_node("/root/Config").paused = false
		
		current_checkpoint_pos = \
			get_node("/root/Config").checkpoint_pos
		current_checkpoint_texts = \
			get_node("/root/Config").checkpoint_texts
		current_checkpoint_cam = \
			get_node("/root/Config").checkpoint_cam
			
		checkpoint_apply()
	
func _process(delta):
	if Input.is_action_just_pressed("pause"):
		get_node("/root/Config").paused = true
		get_node("/root/Config").checkpoint_pos = \
			current_checkpoint_pos
		get_node("/root/Config").checkpoint_texts = \
			current_checkpoint_texts
		get_node("/root/Config").checkpoint_cam = \
			current_checkpoint_cam
			
		get_tree().change_scene("res://Global/Menu/menu.tscn")
		
	if transitioning or transition_finish:
		if $advance.has_node("END") and $advance_timer.is_stopped():
			get_tree().change_scene( \
			"res://Platforming/Level_22/level_22.tscn")
		elif $advance.has_node("REAL_END"):
			$camera/canvasLayer/colorRect.color.a += 0.005
			if $camera/canvasLayer/colorRect.color.a >= 1.0:
				get_tree().change_scene( \
				"res://Platforming/Credits/credits.tscn")
		else:
			$camera/canvasLayer/colorRect.color.a += 0.01
			if self.name == "root_lv20":
				$camera/canvasLayer/colorRect.color.a -= 0.008
			if $camera/canvasLayer/colorRect.color.a > 1:
				$camera/canvasLayer/colorRect.color.a = 1
			if $advance_timer.is_stopped():
				if transition_finish:
					next_level()
				elif transitioning:
					$advance_timer.start(3.0)
					if self.name == "root_lv20":
						$advance_timer.start(12.0)
					transition_finish = true
		
	if !dead:
		$camera.run_inputs($root_foo/foo.global_position, \
		cam, direction)
		
		if has_node("knees_timer"):
			if $knees_timer.is_stopped() and !knees:
				$root_foo/foo.anim = "stand_up"
				knees = true
		
		if $root_foo/foo.global_position.x <= \
		$camera.cam_area.position.x:
			$root_foo/foo.global_position.x = \
			$camera.cam_area.position.x
		if $root_foo/foo.global_position.y <= \
		$camera.cam_area.position.y:
			$root_foo/foo.global_position.y = \
			$camera.cam_area.position.y
			
		if $root_foo/foo.global_position.x >= \
		$camera.cam_area.position.x + $camera.cam_area.size.x:
			$root_foo/foo.global_position.x = \
			$camera.cam_area.position.x + $camera.cam_area.size.x
		
		if $root_foo/foo.global_position.y >= \
		$camera.cam_area.position.y + $camera.cam_area.size.y + 16:
			$root_foo/foo.death()
	elif $death_timer.is_stopped():
		if !current_checkpoint_texts.empty():
			for child in $texts.get_children():
				child.reset()
			if has_node("bubbles"):
				for child in $bubbles.get_children():
					child.reset()
			if has_node("crystals"):
				for child in $crystals.get_children():
					child.reset()
			if has_node("root_carlFight"):
				$root_carlFight.reset()
			checkpoint_apply()
			dead = false
			$root_foo/foo.alive(current_checkpoint_pos)
			tween_out.interpolate_property($music, "volume_db", \
				-25, -8.0, 1.0, 1, Tween.EASE_IN, 0)
			tween_out.start()
		else:
			get_tree().reload_current_scene()
			
func _input(event):
	if has_node("wasd") or has_node("lshift"):
		if event is InputEventJoypadMotion or \
			event is InputEventJoypadButton:
				if has_node("wasd"):
					$analog.show()
					$wasd.hide()
				else:
					$lt.show()
					$analog.show()
					$lshift.hide()
					$ijkl.hide()
		elif event is InputEventKey:
			if has_node("wasd"):
					$wasd.show()
					$analog.hide()
			else:
				$lshift.show()
				$ijkl.show()
				$lt.hide()
				$analog.hide()
				
	if event is InputEventJoypadButton or \
		event is InputEventJoypadMotion:
			Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	elif event is InputEventMouseMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			
func get_text_attr(attr : String, data : Node):
	var full : String = data.editor_description
	var sub : = full.split(";")
	
	match attr:
		"sender":
			return sub[0].right(8)
		"delay":
			return float(sub[1].right(8))
		"flip":
			if sub[2].right(8) == "true":
				return true
			else:
				return false
		
func checkpoint_reached(spawn_pos : Vector2, text_spawns : Array, \
cam_index : int) -> void:
	if !dead:
		current_checkpoint_pos = spawn_pos
		current_checkpoint_texts = text_spawns
		current_checkpoint_cam = cam_index
	
func checkpoint_apply():
	var new_cam = $cam_changes.get_child(current_checkpoint_cam)
	
	new_cam.override_signal()
	
	for text in current_checkpoint_texts:
		$texts.get_child(text - 1).reset()
		$texts.get_child(text - 1).init()
	
func cam_changed(cam_pos, new_dir):
	cam = cam_pos
	direction = new_dir
	$camera.cam_area = cam
	
# took out ghost_text array bc i'm not using bubbles
func bubble_collision(collision, position):
	pass
#	for text in ghost_texts:
#		if collision == text:
#			text.bounced(collision, position)
			
func text_appear(sent : bool) -> void:
	if sent:
		$camera/send.play()
	else:
		$camera/receive.play()
	
func restart() -> void:
	dead = true
	tween_out.interpolate_property($music, "volume_db", \
		-8.0, -25, 1.0, 1, Tween.EASE_IN, 0)
	tween_out.start()
	if !current_checkpoint_texts.empty():
		$root_foo/foo.position = current_checkpoint_pos
	$death_timer.start(DEATH_TIME)
	
func transition(level : int) -> void:
	if $advance.has_node("END"):
		$music.stop()
		$root_foo/foo/glass.play()
		$camera/canvasLayer/colorRect.color.a = 1.0
		$advance_timer.start(8.0)
		transitioning = true
	elif $advance.has_node("REAL_END"):
		if !transitioning:
			$camera/door.play()
			$root_foo/foo.control = false
			$root_foo/foo.velocity = Vector2(0, 0)
			$root_foo/foo.anim = "door"
			transitioning = true
	else:
		to_level = level
		tween_out.interpolate_property($music, "volume_db", \
			-8.0, -80, 4.0, 1, Tween.EASE_IN, 0)
		tween_out.start()
		transitioning = true
	
func next_level() -> void:
	if to_level > 3 and to_level != 17 and to_level != 19 and \
	to_level != 20 and to_level != 21:
		var next_scene : String = \
		"res://Platforming/Level_" + str(to_level - 1) + \
		"/level_" + str(to_level - 1) + "_insta.tscn"
		get_node("/root/Config").level = next_scene
		get_node("/root/Config").save()
		get_tree().change_scene(next_scene)
	else:
		var next_scene : String = \
		"res://Platforming/Level_" + str(to_level) + \
		"/level_" + str(to_level) + "_lock.tscn"
		get_node("/root/Config").level = next_scene
		get_node("/root/Config").save()
		get_tree().change_scene(next_scene)
