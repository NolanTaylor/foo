extends Area2D

var player_body
var emitted : bool = false

signal enter(to_level)

func set_body(body):
	player_body = body

func _ready():
	pass

func _process(delta):
	if !emitted:
		if overlaps_body(player_body):
			emit_signal("enter", int(editor_description))
