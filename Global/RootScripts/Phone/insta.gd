extends Control

onready var tween_out = get_node("tween")

var alissa_pfp = preload("res://Assets/icons/alissa_pfp.png")
var ashe_pfp = preload("res://Assets/icons/ashe_pfp.png")
var bar_pfp = preload("res://Assets/icons/bar_pfp.png")
var hana_pfp = preload("res://Assets/icons/hana_pfp.png")

var transitioning : bool = false
var to_level : String

func _ready():
	to_level  = self.editor_description
	
	if get_node("/root/Config").paused:
		get_tree().change_scene( \
		"res://Platforming/Level_" + to_level + \
		"/level_" + to_level + ".tscn")
		
	tween_out.interpolate_property($music, "volume_db", \
		-80, -8.0, 2.0, 1, Tween.EASE_IN, 0)
	tween_out.start()
	$music.play()
	
	$timer.one_shot = true
	
	$App.max_scroll = $preload.max_scroll
	
	# will need to loop for an array of posts
	for post in $preload.posts:
		var post_new = post[0].instance()
		
		post_new.get_node("Content/TextureRect/Sprite"). \
			texture = post[1]
		post_new.get_node("Content/TextureRect/Sprite"). \
			position = Vector2(640, 0)
		if post[2] == "Alissa":
			post[2] = "Kerry"
		post_new.get_node("Content/Title/Label"). \
			text = post[2]
		post_new.get_node( \
			"Content/VBoxContainer/ButtonContainer2/ViewsLabel"). \
				text = post[3]
		post_new.get_node( \
			"Content/VBoxContainer/ButtonContainer3/Description"). \
				bbcode_text = post[4]
		post_new.get_node( \
			"Content/VBoxContainer/ButtonContainer4/ViewsLabel2"). \
				text = post[5]
		
		match post[2]:
			"Kerry":
				post_new.get_node("Content/Title/TextureRect"). \
					texture = alissa_pfp
			"Ashe":
				post_new.get_node("Content/Title/TextureRect"). \
					texture = ashe_pfp
			"Bar":
				post_new.get_node("Content/Title/TextureRect"). \
					texture = bar_pfp
			"Hana":
				post_new.get_node("Content/Title/TextureRect"). \
					texture = hana_pfp
		
		# /* ---------------------------------------------- */
		# this part is only for the placeholder sprite
		# the real sprites will already be scaled (hopefully)
		
#		post_new.get_node("Content/TextureRect/Sprite"). \
#		scale = Vector2(0.5, 0.5)
		
		# /* ---------------------------------------------- */
		
		$App/CurrentView/Home.get_node(\
		"VBoxContainer2/ScrollContainer/VBoxContainer"). \
		add_child(post_new)
	
func _process(delta):
	if transitioning and $timer.is_stopped():
		get_tree().change_scene( \
		"res://Platforming/Level_" + to_level + \
		"/level_" + to_level + "_lock.tscn")
	elif transitioning:
		if $ColorRect.color.a > 1.0:
			$ColorRect.color.a = 1.0
		else:
			$ColorRect.color.a += 0.25 * delta
	
func next_level() -> void:
	$timer.start(6)
	tween_out.interpolate_property($music, "volume_db", \
		-8.0, -80, 6.0, 1, Tween.EASE_IN, 0)
	tween_out.start()
	transitioning = true
