extends Node2D

var ready : bool = false
var to_level : String

func _ready():
	$timer.one_shot = true
	$timer.start(int($timer.editor_description))
	
	$canvasLayer/colorRect.color.a = 1.0
	
	$root_foo.hide()
	$root_foo/foo.control = false
	$root_foo/foo.gravity = false
	
	if get_node("/root/Config").light_mode:
		$lock_screen.texture = load("res://Assets/lockscreen_light.png")
		$wallpaper.texture = load("res://Assets/wallpaper_light.png")
	
	to_level = $lock_screen.editor_description
	
func _process(delta):
	if $timer.is_stopped():
		if ready:
			get_tree().change_scene( \
			"res://Platforming/Level_" + to_level + \
			"/level_" + to_level + ".tscn")
		else:
			$audio.play()
			$timer.start(1.2)
			$root_foo.show()
			$root_foo/foo.gravity = true
			ready = true
	else:
		$canvasLayer/colorRect.color.a -= 0.05
		if $canvasLayer/colorRect.color.a <= 0:
			$canvasLayer/colorRect.color.a = 0
