extends Node

var config = ConfigFile.new()
var err = config.load("user://save.cfg")

var level : String = "res://Platforming/Level_1/level_1_lock.tscn"

var paused : bool = false

var checkpoint_pos : Vector2
var checkpoint_texts : Array
var checkpoint_cam : int

var left
var right
var up
var down
var jump
var dash
var restart
var pause
var cam_left
var cam_right
var cam_up
var cam_down

var light_mode : bool = false
var fullscreen : bool = false

var master_volume : float = 1.0
var music_volume : float = 0.5
var sound_fx_volume : float = 0.5

var invincibility : bool = false
var infinite_dash : bool = false

func _ready():
	if err == OK:
		print("yee haw")
	else:
		print("nee naw")
		
		var file = File.new()
		file.open("user://save.cfg", File.WRITE)
		err = config.load("user://save.cfg")
	
	if config.has_section_key("Level", "Level"):
		level = config.get_value("Level", "Level")
	if config.has_section_key("Control", "Left"):
		left = config.get_value("Control", "Left")
	if config.has_section_key("Control", "Right"):
		right = config.get_value("Control", "Right")
	if config.has_section_key("Control", "Up"):
		up = config.get_value("Control", "Up")
	if config.has_section_key("Control", "Down"):
		down = config.get_value("Control", "Down")
	if config.has_section_key("Control", "Jump"):
		jump = config.get_value("Control", "Jump")
	if config.has_section_key("Control", "Dash"):
		dash = config.get_value("Control", "Dash")
	if config.has_section_key("Control", "Restart"):
		restart = config.get_value("Control", "Restart")
	if config.has_section_key("Control", "Pause"):
		pause = config.get_value("Control", "Pause")
	if config.has_section_key("Control", "Cam_Left"):
		cam_left = config.get_value("Control", "Cam_Left")
	if config.has_section_key("Control", "Cam_Right"):
		cam_right = config.get_value("Control", "Cam_Right")
	if config.has_section_key("Control", "Cam_Up"):
		cam_up = config.get_value("Control", "Cam_Up")
	if config.has_section_key("Control", "Cam_Down"):
		cam_down = config.get_value("Control", "Cam_Down")
		
	if config.has_section_key("Display", "Light"):
		light_mode = config.get_value("Display", "Light")
	if config.has_section_key("Display", "Fullscreen"):
		fullscreen = config.get_value("Display", "Fullscreen")
		
	if config.has_section_key("Sound", "Master"):
		master_volume = config.get_value("Sound", "Master")
	if config.has_section_key("Sound", "Music"):
		music_volume = config.get_value("Sound", "Music")
	if config.has_section_key("Sound", "Sound_FX"):
		sound_fx_volume = config.get_value("Sound", "Sound_FX")
	
	if config.has_section_key("Accessibility", "Invincibility"):
		invincibility = config.get_value("Accessibility", "Invincibility")
	if config.has_section_key("Accessibility", "Infinite_Dash"):
		infinite_dash = config.get_value("Accessibility", "Infinite_Dash")
		
func reset():
	level = "res://Platforming/Level_1/level_1_lock.tscn"
	save()
	
func save():
	if err == OK:
		config.set_value("Level", "Level", level)
		
		config.set_value("Control", "Left", left)
		config.set_value("Control", "Right", right)
		config.set_value("Control", "Up", up)
		config.set_value("Control", "Down", down)
		config.set_value("Control", "Jump", jump)
		config.set_value("Control", "Dash", dash)
		config.set_value("Control", "Restart", restart)
		config.set_value("Control", "Pause", pause)
		config.set_value("Control", "Cam_Left", cam_left)
		config.set_value("Control", "Cam_Right", cam_right)
		config.set_value("Control", "Cam_Up", cam_up)
		config.set_value("Control", "Cam_Down", cam_down)
		
		config.set_value("Display", "Light", light_mode)
		config.set_value("Display", "Fullscreen", fullscreen)
		
		config.set_value("Sound", "Master", master_volume)
		config.set_value("Sound", "Music", music_volume)
		config.set_value("Sound", "Sound_FX", sound_fx_volume)
		
		config.set_value("Accessibility", "Invincibility", invincibility)
		config.set_value("Accessibility", "Infinite_Dash", infinite_dash)
		
		config.save("user://save.cfg")
	else:
		print("nee naw 2.0")
