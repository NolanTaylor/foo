extends Area2D

var player_body

signal change(cam_pos)

func set_body(body):
	player_body = body
	
func convert_to_rect(end_pos : Vector2) -> Rect2:
	var rect : Rect2
	
	rect.position = self.global_position
	rect.size = Vector2(end_pos.x, end_pos.y)
	
	return rect
	
func override_signal() -> void:
	var rect : Rect2 = convert_to_rect($cam_rect.position)
	var direction : String = $cam_rect.editor_description
	emit_signal("change", rect, direction)
	
func _ready():
	pass
	
func _process(delta):
	if overlaps_body(player_body):
		var rect : Rect2 = convert_to_rect($cam_rect.position)
		var direction : String = $cam_rect.editor_description
		emit_signal("change", rect, direction)
