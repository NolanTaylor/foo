extends Area2D

var player_body

signal check(pos, texts, cam)

func set_body(body):
	player_body = body

func _ready():
	pass

func _process(delta):
	if overlaps_body(player_body):
		var spawn_pos : Vector2 = global_position
		var text_spawns : Array
		var spawn_str : = $data.editor_description.split("|")[0]
		var cam_index : = $data.editor_description.split("|")[1]
		for i in spawn_str.split(","):
			text_spawns.append(int(i))
		emit_signal("check", spawn_pos, text_spawns, int(cam_index))
