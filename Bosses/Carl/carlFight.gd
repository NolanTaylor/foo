extends Node2D

const OFFSET : Vector2 = Vector2(4384, 312)

const pattern1 : PoolVector2Array = PoolVector2Array([
	Vector2(32, 32),
	Vector2(288, 32),
])

const pattern2 : PoolVector2Array = PoolVector2Array([
	Vector2(32, 32),
	Vector2(288, 32),
	Vector2(64, 168),
	Vector2(256, 168),
])

const pattern3 : PoolVector2Array = PoolVector2Array([
	Vector2(64, 32),
	Vector2(96, 32),
	Vector2(128, 32),
	Vector2(160, 32),
	Vector2(192, 32),
	Vector2(224, 32),
	Vector2(256, 32),
])

const pattern4 : PoolVector2Array = PoolVector2Array([
	Vector2(32, 168),
	Vector2(288, 168),
	Vector2(32, 32),
	Vector2(288, 32),
])

const pattern5 : PoolVector2Array = PoolVector2Array([
	Vector2(32, 32),
	Vector2(32, 64),
	Vector2(32, 96),
	Vector2(32, 128),
	Vector2(32, 160),
	Vector2(32, 192),
])

const pattern6 : PoolVector2Array = PoolVector2Array([
	Vector2(288, 32),
	Vector2(288, 64),
	Vector2(288, 96),
	Vector2(288, 128),
	Vector2(288, 160),
	Vector2(288, 192),
])

const pattern7 : PoolVector2Array = PoolVector2Array([
	Vector2(32, 80),
	Vector2(96, 48),
	Vector2(224, 48),
	Vector2(288, 80),
	Vector2(288, 112),
	Vector2(224, 144),
	Vector2(96, 144),
	Vector2(32, 112),
])

var battle : bool = false
var act : int = 0
var carls_gone : int = 0
var foo : KinematicBody2D
var rng = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	
	$timer.one_shot = true
	
	foo = get_parent().get_node("root_foo/foo")
	
	for child in $carls.get_children():
		child.set_id(foo.get_instance_id())
		child.connect("hit", foo, "death")
		child.connect("despawned", self, "carl_gone")
	
func _process(delta):
	if $battleStart.overlaps_body(foo) and act == 0:
		act += 1
		
	for child in $carls.get_children():
		child.run_inputs(foo.global_position)
		
	match act:
		1:
			$timer.start(1.0)
			act += 1
		2:
			if $timer.is_stopped():
				for i in range(2):
					$carls.get_children()[i].reset( \
					pattern1[i], -1)
				carls_gone = 0
				act += 1
		3:
			if carls_gone == 2:
				for i in range(4):
					$carls.get_children()[i].reset( \
					pattern2[i], (i + 1) * 0.3)
				carls_gone = 0
				act += 1
		4:
			if carls_gone == 4:
				for i in range(8):
					$carls.get_children()[i].reset( \
					Vector2(160, 32), (i + 1) * 0.6)
				carls_gone = 0
				act += 1
		5:
			if carls_gone == 4:
				for i in range(4):
					$carls.get_children()[i].reset( \
					pattern2[i], (i + 1) * 0.3)
				carls_gone = 0
				act += 1
		6:
			if carls_gone == 8:
				for i in range(7):
					$carls.get_children()[i].reset( \
					pattern3[i], (i + 1) * 0.2)
				# make rumbly or smth idk
				$timer.start(4.0)
				carls_gone = 0
				act += 1
		7:
			if $timer.is_stopped():
				get_parent().get_node("texts/text54").move(4480, 40)
				get_parent().get_node("texts/text55").move(4600, -40)
				$timer.start(4.0)
				act += 1
		8:
			if $timer.is_stopped():
				for i in range(2):
					$carls.get_children()[i].reset( \
					pattern4[i], -1)
				get_parent().get_node("texts/text53").hide()
				get_parent().get_node( \
				"texts/text53/collisionShape").disabled = true
				carls_gone = 0
				act += 1
		9:
			if carls_gone == 2:
				for i in range(2):
					$carls.get_children()[i].reset( \
					pattern4[i + 2], -1)
				carls_gone = 0
				act += 1
		10:
			if carls_gone == 2:
				for i in range(6):
					$carls.get_children()[i].reset( \
					pattern5[i], -1)
				carls_gone = 0
				act += 1
		11:
			if carls_gone == 6:
				for i in range(6):
					$carls.get_children()[i].reset( \
					pattern6[i], -1)
				carls_gone = 0
				act += 1
		12:
			if carls_gone == 6:
				for i in range(4):
					$carls.get_children()[i].reset( \
					pattern4[i], (i + 1) * 0.4)
				carls_gone = 0
				act += 1
		13:
			if carls_gone == 4:
				get_parent().get_node("cam_changes/cam12"). \
				emit_signal("change", get_parent(). \
				get_node("cam_changes/cam12").get_instance_id())
		_:
			pass
			
func carl_gone() -> void:
	carls_gone += 1
	
func reset() -> void:
	act = 0
	get_parent().get_node("texts/text54").position = Vector2(4384, 328)
	get_parent().get_node("texts/text55").position = Vector2(4696, 328)
	
	for child in $carls.get_children():
		child.hide()
		child.get_node("collisionShape").disabled = true
		child.get_node("particles_left").hide()
		child.get_node("particles_left").emitting = false
		child.get_node("particles_right").hide()
		child.get_node("particles_right").emitting = false
		child.velocity = Vector2(0, 0)
		child.draw = true
		child.active = false
		child.spawn = false
