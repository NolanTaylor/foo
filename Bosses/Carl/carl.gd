extends Area2D

const MAX_VELOCITY : int = 480
const LEFT_WALL : int = 4392
const RIGHT_WALL : int = 4696
const TOP_WALL : int = 328
const BOTTOM_WALL : int = 512
const ACCELERATION : float = 30.0
const BACC_A : float = 15.0
const STARTING_VELOCITY : float = 240.0
const SPIN_TIME : float = 1.0

var draw : bool = true
var active : bool = false
var spawn : bool = false
var act : int
var player_id : int
var tip_length : int
var d : float
var delta_x : float
var slope : float
var mod : float = 1.0
var velocity : Vector2
var v_mod : Vector2
var spike_mod : Vector2
var player_pos : Vector2
var path : Vector2

signal hit
signal despawned

func set_id(id : int) -> void:
	player_id = id
	
func reset(new_pos : Vector2, time : float):
	if time != -1:
		$spawn_timer.start(time)
		spawn = true
	else:
		show()
		active = true
		draw = true
	act = 0
	position = new_pos
	$particles_left.hide()
	$particles_left.emitting = false
	$particles_right.hide()
	$particles_right.emitting = false
	
func _ready():
	$spin_timer.one_shot = true
	$spawn_timer.one_shot = true
	$collisionShape.disabled = true
	
	tip_length = ($collisionShape.shape.height / 2) + \
	$collisionShape.shape.radius
	
func _process(delta):
	if active:
		position += velocity * delta
			
		run_spike()
	elif spawn:
		if $spawn_timer.is_stopped():
			show()
			active = true
			draw = true
			spawn = false
	
func run_inputs(pos):
	player_pos = pos
	
func run_spike():
	match act:
		0:
			$spin_timer.start(SPIN_TIME)
			spike_mod = Vector2(1, 1)
			rotation = 0
			act += 1
		1:
			rotate(PI / 8)
			
			if $spin_timer.is_stopped():
				act += 1
		2:
			$collisionShape.disabled = false
			
			path = global_position - player_pos
			slope = path.y / path.x
			
			var angle : float = atan(path.x / path.y)
			
			if path.y < 0:
				rotation = -((PI / 2) + angle)
			else:
				rotation = (PI / 2) - angle
				
			d = STARTING_VELOCITY
				
			act += 1
		3:
			delta_x = sqrt(abs(pow(d, 2) / (pow(slope, 2) + 1)))
			
			if path.x > 0:
				delta_x *= -1
				
			delta_x *= -1
			
			velocity = Vector2(delta_x, delta_x * slope)
			
			d -= BACC_A
			
			if d <= 0:
				act += 1
		4:
			if d < MAX_VELOCITY:
				d += ACCELERATION
			
			delta_x = sqrt(abs(pow(d, 2) / (pow(slope, 2) + 1)))
			
			if path.x > 0:
				delta_x *= -1
				
			velocity = Vector2(delta_x, delta_x * slope)
			velocity *= spike_mod
			
			var tip : Vector2 = Vector2(0, 0)
			
			tip.x = global_position.x - (cos(rotation) * tip_length)
			tip.y = global_position.y - (sin(rotation) * tip_length)
			
			if d >= MAX_VELOCITY:
				if tip.x <= LEFT_WALL:
					global_position.x = \
					LEFT_WALL + (global_position.x - tip.x)
					velocity = Vector2(0, 0)
					act += 1
				elif tip.x >= RIGHT_WALL:
					global_position.x = \
					RIGHT_WALL + (global_position.x - tip.x)
					velocity = Vector2(0, 0)
					act += 1
					
				if tip.y <= TOP_WALL:
					global_position.y = \
					TOP_WALL + (global_position.y - tip.y)
					velocity = Vector2(0, 0)
					act += 1
				elif tip.y >= BOTTOM_WALL:
					global_position.y = \
					BOTTOM_WALL + (global_position.y - tip.y)
					velocity = Vector2(0, 0)
					act += 1
		5:
			act += 1
			draw = false
			$collisionShape.disabled = true
			$particles_left.show()
			$particles_left.emitting = true
			$particles_right.show()
			$particles_right.emitting = true
			update()
		6:
			if !$particles_left.emitting and \
			!$particles_right.emitting:
				hide()
				emit_signal("despawned")
				active = false
				
func _draw():
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	var size : Vector2 = Vector2($collisionShape.shape.height + \
	$collisionShape.shape.radius * 2, \
	$collisionShape.shape.radius * 2)
	
	if !draw:
		size = Vector2(0, 0)

	style_box.bg_color = Color("147efb")
	style_box.set_corner_radius_all($collisionShape.shape.radius)
	draw_style_box(style_box, Rect2(Vector2( \
	-($collisionShape.shape.height / 2 + \
	$collisionShape.shape.radius), \
	-$collisionShape.shape.radius), \
	size))
	
func _on_root_carl_body_entered(body):
	if body.get_instance_id() == player_id:
		emit_signal("hit")
