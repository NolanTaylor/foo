extends KinematicBody2D

var target = Vector2(0, 0)
var velocity = Vector2(0, 0)
var active = false
var player_body_id = 0

signal hit

func _ready():
	hide()
	
func set_player_id(id):
	player_body_id = id

func set_target(pos):
	target = pos
	active = true

	velocity = Vector2(target.x - position.x, target.y - position.y)
	$spell_particles.emitting = true

func spawn(pos, flip):
	if flip:
		position = pos + Vector2(50, -20)
	else:
		position = pos + Vector2(-50, -20)
		
	show()

func parried(body, pos):
	if body == self:
		if position.x <= pos.x:
			velocity.x = -160
		elif position.x > pos.x:
			velocity.x = 160

		if position.y <= pos.y:
			velocity.y = -160
		elif position.y > pos.y:
			velocity.y = 160

func _physics_process(delta):
	if active:
		var collision = move_and_collide(velocity * delta)
		
		if collision:
			on_hit()

			if collision.collider_id == player_body_id:
				emit_signal("hit")
		elif position.x <= 80 or position.x >= 540:
			on_hit()
		
	$spell_sprite.play()

func on_hit():
	get_parent().get_node("alissa").return_state()
	position = Vector2(0, 0)
	active = false
	$spell_particles.emitting = false
	$spell_particles.restart()
	hide()
