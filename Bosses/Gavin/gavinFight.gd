extends Node2D

const DEATH_TIME = 2

var dead : bool = false

func _ready():
	$death_timer.one_shot = true
	
	$root_gavin/gavin/hand_1.set_player_id($root_foo/foo.get_instance_id())
	$root_gavin/gavin/hand_1.set_floor_id($TileMap.get_instance_id())
	$root_gavin/gavin/hand_1.connect("hit", $root_foo/foo, "death")
	$root_gavin/gavin/hand_1.connect("shookth", $root_foo/foo, "shook")
	$root_gavin/gavin/hand_1.connect("parried", $root_foo/foo, "grace")
	$root_gavin/gavin/hand_1.connect("bounce", $root_foo/foo, "bounce")
	$root_gavin/gavin/hand_2.set_player_id($root_foo/foo.get_instance_id())
	$root_gavin/gavin/hand_2.set_floor_id($TileMap.get_instance_id())
	$root_gavin/gavin/hand_2.connect("hit", $root_foo/foo, "death")
	$root_gavin/gavin/hand_2.connect("shookth", $root_foo/foo, "shook")
	$root_gavin/gavin/hand_2.connect("parried", $root_foo/foo, "grace")
	$root_gavin/gavin/hand_2.connect("bounce", $root_foo/foo, "bounce")

	$root_foo/foo.set_id([$root_gavin/gavin/hand_1.get_instance_id(), $root_gavin/gavin/hand_2.get_instance_id()])
	$root_foo/foo.combat = true
	$root_foo/foo.connect("dead", self, "restart")
	$root_foo/foo/smacc.set_objects([$root_gavin/gavin], [])
	$root_foo/foo/smacc.connect("hit", $root_gavin/gavin, "stagger")
	$root_foo/foo/parry.set_objects([$root_gavin/gavin/hand_1, $root_gavin/gavin/hand_2], [$root_gavin/spell], [0, 1], [0])
	$root_foo/foo/parry.connect("parry", $root_gavin/spell, "parried")
	$root_foo/foo/parry.connect("parry", $root_gavin/gavin/hand_1, "parried")
	$root_foo/foo/parry.connect("parry", $root_gavin/gavin/hand_2, "parried")
	
	$root_gavin/spell.set_player_id($root_foo/foo.get_instance_id())
	$root_gavin/spell.connect("hit", $root_foo/foo, "death")

func _process(delta):
	$root_gavin/gavin.run_inputs($root_foo/foo.global_position)
	
	if $death_timer.is_stopped() and dead:
		dead = false
		$root_foo/foo.alive()
	
func restart():
	dead = true
	$death_timer.start(DEATH_TIME)
