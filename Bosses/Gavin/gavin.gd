extends Node2D

const ACCELERATION = 20
const MAX_SPEED = 60
const HOVER_MARGIN = 50
const MARGIN = 10
const CEILING = 70
const LEFT_WALL = 80
const RIGHT_WALL = 240
const MODE_TIME = 2.5
const FRICTION = 0.1

var rng = RandomNumberGenerator.new()

var mode : String = "follow"
var anim : String = "idle"
var bash_bool : int # not boolean?
var pos_v : Vector2
var velocity : Vector2
var target_pos : Vector2
var player_pos : Vector2

func stagger(object, direction):
	match direction:
		"north":
			pos_v.y -= 100
		"south":
			pos_v.y += 100
		"east":
			pos_v.x += 100
		"west":
			pos_v.x -= 100

	anim = "hit"

func _ready():
	rng.randomize()
	
	target_pos = position
	
	$mode_timer.one_shot = true
	
	$mode_timer.start(MODE_TIME)

func _process(delta):
	$gavin_sprite.play("idle")

	position += pos_v * delta
	target_pos += velocity * delta

	if target_pos.y < CEILING:
		target_pos.y = CEILING
		
	if target_pos.x < LEFT_WALL:
		target_pos.x = LEFT_WALL
		
	if target_pos.x > RIGHT_WALL:
		target_pos.x = RIGHT_WALL
		
	pos_v.x = lerp(pos_v.x, 0, 0.1)
	pos_v.y = lerp(pos_v.y, 0, 0.1)
	position.x = lerp(position.x, target_pos.x, 0.1)
	position.y = lerp(position.y, target_pos.y, 0.1)
	
	match mode:
		"follow":
			run_follow()
		"find_right":
			run_find_right()
		"find_left":
			run_find_left()
		"smash":
			run_smash()
		"left_bash":
			run_left_bash()
		"right_bash":
			run_right_bash()
		"clash":
			run_clash()
		"vertical_return":
			run_vertical_return()
		"horizontal_return":
			run_horizontal_return()

	$gavin_sprite.play(anim)
	
	if anim == "hit":
		anim = "idle"

func run_inputs(pos):
	player_pos = pos

func next_mode(attack):
	match attack:
		"smash":
			bash_bool = rng.randi_range(1, 2)

			if bash_bool == 1:
				mode = "find_left"
			else:
				mode = "find_right"
		"bash":
			mode = "follow"

	if player_pos.x < position.x + 25 and player_pos.x > position.x - 25:
		if player_pos.y < position.y - 5 and player_pos.y > position.y - 60:
			mode = "clash"

func run_follow():
	if target_pos.x > player_pos.x + MARGIN:
		velocity.x = max(velocity.x - ACCELERATION, -MAX_SPEED)
	elif target_pos.x < player_pos.x - MARGIN:
		velocity.x = min(velocity.x + ACCELERATION, MAX_SPEED)
	else:
		velocity.x = lerp(velocity.x, 0, FRICTION)

	if target_pos.y > player_pos.y - HOVER_MARGIN + MARGIN:
		velocity.y = max(velocity.y - ACCELERATION, -MAX_SPEED)
	elif target_pos.y < player_pos.y - HOVER_MARGIN - MARGIN:
		velocity.y = min(velocity.y + ACCELERATION, MAX_SPEED)
	else:
		velocity.y = lerp(velocity.y, 0, FRICTION)
		
	if $mode_timer.is_stopped():
		velocity = Vector2(0, 0)
		mode = "smash"

		if player_pos.x < target_pos.x + 25 and player_pos.x > target_pos.x - 25:
			if player_pos.y < target_pos.y - 5 and player_pos.y > target_pos.y - 60:
				$hand_1.flip = false
				mode = "clash"

func run_find_right():
	if target_pos.x < RIGHT_WALL:
		velocity.x = min(velocity.x + ACCELERATION, MAX_SPEED)

	if target_pos.y > player_pos.y - HOVER_MARGIN + MARGIN:
		velocity.y = max(velocity.y - ACCELERATION, -MAX_SPEED)
	elif target_pos.y < player_pos.y - HOVER_MARGIN - MARGIN:
		velocity.y = min(velocity.y + ACCELERATION, MAX_SPEED)
	else:
		velocity.y = lerp(velocity.y, 0, FRICTION)

	if $hand_1.global_position.y > player_pos.y + MARGIN:
		$hand_1.velocity.y = max($hand_1.velocity.y - ACCELERATION, -MAX_SPEED)
	elif $hand_1.global_position.y < player_pos.y - MARGIN:
		$hand_1.velocity.y = min($hand_1.velocity.y + ACCELERATION, MAX_SPEED)
	else:
		$hand_1.velocity.y = lerp($hand_1.velocity.y, 0, FRICTION)

	if $hand_1.position.y > 45:
		$hand_1.position.y = 45

	if target_pos.x >= RIGHT_WALL:
		velocity.x = 0

	if $mode_timer.is_stopped():
		velocity = Vector2(0, 0)
		$hand_1.velocity = Vector2(0, 0)
		mode = "left_bash"

func run_find_left():
	if target_pos.x > LEFT_WALL:
		velocity.x = max(velocity.x - ACCELERATION, -MAX_SPEED)

	if target_pos.y > player_pos.y - HOVER_MARGIN + MARGIN:
		velocity.y = max(velocity.y - ACCELERATION, -MAX_SPEED)
	elif target_pos.y < player_pos.y - HOVER_MARGIN - MARGIN:
		velocity.y = min(velocity.y + ACCELERATION, MAX_SPEED)
	else:
		velocity.y = lerp(velocity.y, 0, FRICTION)

	if $hand_2.global_position.y > player_pos.y + MARGIN:
		$hand_2.velocity.y = max($hand_2.velocity.y - ACCELERATION, -MAX_SPEED)
	elif $hand_2.global_position.y < player_pos.y - MARGIN:
		$hand_2.velocity.y = min($hand_2.velocity.y + ACCELERATION, MAX_SPEED)
	else:
		$hand_2.velocity.y = lerp($hand_2.velocity.y, 0, FRICTION)

	if $hand_2.position.y > 45:
		$hand_2.position.y = 45

	if target_pos.x <= LEFT_WALL:
		velocity.x = 0

	if $mode_timer.is_stopped():
		velocity = Vector2(0, 0)
		$hand_2.velocity = Vector2(0, 0)
		mode = "right_bash"

func run_left_bash():
	if $hand_1.left_bash():
		mode = "horizontal_return"

func run_right_bash():
	if $hand_2.right_bash():
		mode = "horizontal_return"

func run_smash():
	$hand_1/hand_sprite.animation = "fist"
	$hand_2/hand_sprite.animation = "fist"
	
	if $hand_1.flip:
		$hand_1.velocity.y += 5
	else:
		$hand_1.velocity.y -= 2
		
	if $hand_2.flip:
		$hand_2.velocity.y += 5
	else:
		$hand_2.velocity.y -= 2
	
	if $hand_1.velocity.y <= -50:
		$hand_1.flip = true
	
	if $hand_2.velocity.y <= -50:
		$hand_2.flip = true
		
	if $hand_1.collided and $hand_2.collided:
		$hand_1.flip = false
		$hand_1.velocity = Vector2(0, 0)
		$hand_1/hand_sprite.animation = "palm"
		$hand_2.flip = false
		$hand_2.velocity = Vector2(0, 0)
		$hand_2/hand_sprite.animation = "palm"
		mode = "vertical_return"

func run_clash():
	$hand_1/hand_sprite.animation = "fist"
	$hand_2/hand_sprite.animation = "fist"

	if $hand_1.flip:
		$hand_1.velocity.x -= 5
		$hand_2.velocity.x += 5
	else:
		$hand_1.velocity.x += 2
		$hand_2.velocity.x -= 2

	if $hand_1.global_position.y > player_pos.y:
		$hand_1.velocity.y -= 2
		$hand_2.velocity.y -= 2
	elif $hand_1.global_position.y < player_pos.y:
		$hand_1.velocity.y += 2
		$hand_2.velocity.y += 2

	if $hand_1.velocity.x >= 50:
		$hand_1.flip = true
		$hand_1.velocity.y = 0
		$hand_2.velocity.y = 0

	if $hand_1.velocity.x <= -180:
		$hand_1.velocity = Vector2(0, 0)
		$hand_1.flip = false
		$hand_1/hand_sprite.animation = "palm"
		$hand_2.velocity = Vector2(0, 0)
		$hand_2.flip = false
		$hand_2/hand_sprite.animation = "palm"
		mode = "horizontal_return"
		
func run_vertical_return():
	var flag : Array = [false, false]
	
	if $hand_1.vertical_return():
		flag[0] = true
	if $hand_2.vertical_return():
		flag[1] = true
		
	if flag[0] and flag[1]:
		$mode_timer.start(MODE_TIME)
		next_mode("smash")

func run_horizontal_return():
	var flag : Array = [false, false]
	
	if $hand_1.horizontal_return():
		flag[0] = true
	if $hand_2.horizontal_return():
		flag[1] = true
	
	if flag[0] and flag[1]:
		$mode_timer.start(MODE_TIME)
		next_mode("bash")
