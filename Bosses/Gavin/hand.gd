extends Area2D

const ACC_RATIO = 12.0
const RETURN_ACCELERATION = 5
const BASH_ACCELERATION = 5

var flip : bool = false
var collided : bool = false
var rest_pos : Vector2 = Vector2(0, 0)
var slope : Vector2 = Vector2(0, 0)
var velocity : Vector2 = Vector2(0, 0)
var acceleration : Vector2 = Vector2(0, 0)
var player_body_id : int = 0
var floor_body_id : int = 0

signal hit
signal shookth
signal parried
signal bounce(v)

func _ready():
	rest_pos = position
	
func set_player_id(id):
	player_body_id = id

func set_floor_id(id):
	floor_body_id = id

func _process(delta):
	position += velocity * delta
	
func left_bash():
	$hand_sprite.animation = "fist"

	if flip:
		velocity.x -= BASH_ACCELERATION
	else:
		velocity.x += 2

	if velocity.x >= 50:
		flip = true

	if global_position.x <= get_parent().LEFT_WALL:
		$hand_sprite.animation = "palm"
		flip = false
		return true
	else:
		return false
	
func right_bash():
	$hand_sprite.animation = "fist"

	if flip:
		velocity.x += BASH_ACCELERATION
	else:
		velocity.x -= 2

	if velocity.x <= -50:
		flip = true

	if global_position.x >= get_parent().RIGHT_WALL:
		$hand_sprite.animation = "palm"
		flip = false
		return true
	else:
		return false
		
func bash():
	pass
		
func clash():
	pass
	
func vertical_return():
	velocity.y -= RETURN_ACCELERATION
		
	if position.y <= rest_pos.y:
		return_state()
		return true
	else:
		return false
	
func horizontal_return():
	if flip:
		if abs(velocity.x) <= 300:
			velocity += acceleration
		else:
			velocity -= acceleration
	else:
		if rest_pos.x - position.x > 0:
			velocity.x += RETURN_ACCELERATION * 5
			
			if velocity.x >= 0:
				slope.x = rest_pos.x - position.x
				slope.y = rest_pos.y - position.y
				acceleration.x = slope.x / ACC_RATIO
				acceleration.y = slope.y / ACC_RATIO
				velocity = Vector2(0, 0)
				flip = true
		elif rest_pos.x - position.x < 0:
			velocity.x -= RETURN_ACCELERATION * 5
			
			if velocity.x <= 0:
				slope.x = rest_pos.x - position.x
				slope.y = rest_pos.y - position.y
				acceleration.x = slope.x / ACC_RATIO
				acceleration.y = slope.y / ACC_RATIO
				velocity = Vector2(0, 0)
				flip = true
		else:
			return_state()
			return true
	
	if slope.x > 0:
		if position.x >= rest_pos.x:
			return_state()
			return true
	elif slope.x < 0:
		if position.x <= rest_pos.x:
			return_state()
			return true
	else:
		return false

func return_state():
	flip = false
	collided = false
	velocity = Vector2(0, 0)
	slope = Vector2(0, 0)
	position = rest_pos

func parried(area, pos):
	if area == self:
		var v_add : Vector2 = Vector2(0, 0)
		var slope_b : Vector2 = Vector2(pos.x - global_position.x, pos.y - global_position.y)
		
		if abs(slope_b.x) >= abs(slope_b.y):
			if slope_b.x >= 0:
				v_add.x += 300
			else:
				v_add.x -= 300
		else:
			if slope_b.y >= 0:
				v_add.y += 300
			else:
				v_add.y -= 300
	
		emit_signal("parried")
		emit_signal("bounce", v_add)

func _on_hand_1_body_entered(body):
	if flip:
		collided = true
		
	if body.get_instance_id() == player_body_id:
		emit_signal("hit")
	elif body.get_instance_id() == floor_body_id and get_parent().mode == "smash":
		velocity = Vector2(0, 0)
		emit_signal("shookth")

func _on_hand_2_body_entered(body):
	if flip:
		collided = true
		
	if body.get_instance_id() == player_body_id:
		emit_signal("hit")
	elif body.get_instance_id() == floor_body_id and get_parent().mode == "smash":
		velocity = Vector2(0, 0)
		emit_signal("shookth")
