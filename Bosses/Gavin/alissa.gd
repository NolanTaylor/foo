extends Area2D

const ACCELERATION = 10
const MAX_SPEED = 80
const MARGIN = 20
const SPELL_TIME = 2.5
const HOLD_TIME = 1

var player_body_id = 0
var velocity = Vector2(0, 0)
var flop = false
var flup = false
var spell_fire = false
var target_set = false
var playing = true
var anim = "NULL"
var wait = "spell"

signal hit

func stagger():
	pass

func _ready():
	anim = "idle"
	$spell_timer.one_shot = true
	$spell_timer.start(SPELL_TIME)
	
func set_player_id(id):
	player_body_id = id

func return_state():
	anim = "idle"
	target_set = false
	spell_fire = false
	$spell_timer.start(SPELL_TIME)

func _process(delta):
	position += velocity * delta

	if $spell_timer.is_stopped():
		if !playing:
			if wait == "spell":
				anim = "raise"
			elif wait == "hold":
				anim = "lower"

		playing = true
		
	if playing:
		$alissa_sprite.play(anim)

func run_inputs(player_pos):
	if flup:
		if wait != "hold":
			velocity.x = min(velocity.x + ACCELERATION, MAX_SPEED)
		else:
			velocity.x = 0
			
		$alissa_sprite.flip_h = true

		if position.x >= 480:
			flup = false
	else:
		if wait != "hold":
			velocity.x = max(velocity.x - ACCELERATION, -MAX_SPEED)
		else:
			velocity.x = 0
			
		$alissa_sprite.flip_h = false

		if position.x <= 160:
			flup = true

	if flop:
		velocity.y += 3

		if velocity.y >= 50:
			flop = false
	else:
		velocity.y -= 3

		if velocity.y <= -50:
			flop = true

	if spell_fire and !target_set:
		get_parent().get_node("spell").set_target(player_pos)
		wait = "spell"
		spell_fire = false
		target_set = true

func _on_alissa_body_entered(body):
	if body.get_instance_id() == player_body_id:
		emit_signal("hit")

func _on_alissa_sprite_animation_finished():
	$alissa_sprite.stop()
	playing = false

	if anim == "raise":
		$spell_timer.start(HOLD_TIME)
		wait = "hold"
		
		get_parent().get_node("spell").spawn(position, $alissa_sprite.flip_h)
	elif anim == "lower":
		playing = true
		spell_fire = true
