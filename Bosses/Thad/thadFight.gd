extends Node2D

const DEATH_TIME : int = 2
const SPAWN_TIME : float = 1.0
const DESPAWN_TIME : float = 0.2

var dead : bool = false
var bubbles : Array
var tentacles : Array
var copies : Array
var foo : KinematicBody2D

func _ready():
	foo = get_parent().get_node("root_foo/foo")
		
	for child in $root_thad/testicles.get_children():
		child.set_time(SPAWN_TIME, DESPAWN_TIME)
		child.set_body(foo)
		child.set_type("thad")
		child.connect("death", foo, "death")
		child.connect("despawned", $root_thad/thad, "despawned")
		
	for child in $root_thad/copies.get_children():
		child.set_id(foo.get_instance_id())
		child.ACCELERATION = $root_thad/thad.ACCELERATION
		child.BACC_A = $root_thad/thad.BACC_A
		child.STARTING_VELOCITY = $root_thad/thad.STARTING_VELOCITY
		child.SPIN_TIME = $root_thad/thad.SPIN_TIME
		child.MAX_VELOCITY = $root_thad/thad.MAX_VELOCITY
		child.TIP_LENGTH = $root_thad/thad.TIP_LENGTH
		child.connect("despawned", $root_thad/thad, "copy_despawned")
		child.connect("hit", foo, "death")
		foo.get_node("smacc").connect("hit", child, "stagger")
		
	$root_thad/thad.set_id(foo.get_instance_id())
	$root_thad/thad.connect("hit", foo, "death")
	
	foo.set_id([$root_thad/thad.get_instance_id()])
	foo.combat = true
	foo.connect("dead", self, "restart")
	foo.get_node("smacc").set_objects(copies + [$root_thad/thad], [])
	foo.get_node("smacc").connect("hit", $root_thad/thad, "stagger")
	foo.get_node("smacc").set_grace($root_thad/thad.GRACE)
	foo.get_node("parry").connect("parry", $root_thad/thad, "parried")

func _process(delta):
	$root_thad/thad.run_inputs(foo.global_position)
