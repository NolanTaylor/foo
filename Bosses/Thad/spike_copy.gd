extends Area2D

var ACCELERATION : float
var BACC_A : float
var STARTING_VELOCITY : float
var SPIN_TIME : float
var MAX_VELOCITY : int
var TIP_LENGTH : int

var anim : String = "to_spike"
var act : int = -1
var player_id : int
var d : float
var delta_x : float
var slope : float
var velocity : Vector2
var player_pos : Vector2
var path : Vector2

signal despawned
signal hit

func set_id(id : int):
	player_id = id

func spawn(pos : Vector2):
	show()
	$copy_collisionPolygon.disabled = true
	position = pos
	act = 0
	
func despawn():
	hide()
	emit_signal("despawned", self)
	act = -1
	
func stagger(object, direction):
	if object == self:
		velocity = Vector2(0, 0)
		$copy_collisionPolygon.disabled = true
		anim = "blink"
		rotation = 0
		act = 40

func _ready():
	hide()
	
	$copy_collisionPolygon.disabled = true
	$spin_timer.one_shot = true

func _process(delta):
	position += velocity * delta
	
	if anim != "NULL":
		$copy_sprite.play(anim)
		
	match act:
		-1:
			pass
		0:
			$spin_timer.start(SPIN_TIME)
			anim = "to_spike"
			rotation = 0
			act += 1
		1:
			rotate(PI / 8)
			
			if $spin_timer.is_stopped():
				$copy_collisionPolygon.disabled = false
				anim = "spike"
				act += 1
		2:
			path = position - player_pos
			slope = path.y / path.x
			
			var angle : float = atan(path.x / path.y)
			
			if path.y < 0:
				rotation = -((PI / 2) + angle)
			else:
				rotation = (PI / 2) - angle
				
			d = STARTING_VELOCITY
				
			act += 1
		3:
			delta_x = sqrt(abs(pow(d, 2) / (pow(slope, 2) + 1)))
			
			if path.x > 0:
				delta_x *= -1
				
			delta_x *= -1
			
			velocity = Vector2(delta_x, delta_x * slope)
			
			d -= BACC_A
			
			if d <= 0:
				act += 1
		4:
			if d < MAX_VELOCITY:
				d += ACCELERATION
			
			delta_x = sqrt(abs(pow(d, 2) / (pow(slope, 2) + 1)))
			
			if path.x > 0:
				delta_x *= -1
				
			velocity = Vector2(delta_x, delta_x * slope)
			
			var tip : Vector2 = Vector2(0, 0)
			
			tip.x = position.x - (cos(rotation) * TIP_LENGTH)
			tip.y = position.y - (sin(rotation) * TIP_LENGTH)
			
			if d >= MAX_VELOCITY:
				if tip.x <= 8:
					position.x = 8 + (position.x - tip.x)
					velocity = Vector2(0, 0)
					act += 1
				elif tip.x >= 312:
					position.x = 312 + (position.x - tip.x)
					velocity = Vector2(0, 0)
					act += 1
					
				if tip.y <= 8:
					position.y = 8 + (position.y - tip.y)
					velocity = Vector2(0, 0)
					act += 1
				elif tip.y >= 192:
					position.y = 192 + (position.y - tip.y)
					velocity = Vector2(0, 0)
					act += 1
		_:
			act += 1
			
			if act > 40:
				if $copy_sprite.frame >= 5:
					despawn()
			elif act == 40:
				$copy_collisionPolygon.disabled = true
				anim = "blink"
				rotation = 0

func _on_spike_copy_body_entered(body):
	if body.get_instance_id() == player_id:
		emit_signal("hit")

func _on_copy_sprite_animation_finished():
	if anim == "to_spike":
		anim = "spike"
