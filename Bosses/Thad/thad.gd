extends Area2D

const ACCELERATION : float = 30.0
const BACC_A : float = 15.0
const STARTING_VELOCITY : float = 240.0
const GRACE : float = 0.025
const SPIN_TIME : float = 1.0
const BLINK_TIME : float = 0.25
const BOUNCE_TIME : int = 6
const MAX_VELOCITY : int = 480
const MAX_BOUNCE_VELOCITY : int = 180
const SMACC : int = 240
const TIP_LENGTH : int = 15
const RISE : int = 40
const STAGGER : int = 200
const HP_MARGIN : int = 192
const MAX_HP : int = 40
const CENTER : Vector2 = Vector2(160, 96)

const form1 : Dictionary = {
	0: {
		"position": Vector2(32, 200),
		"rotation": 0,
		"scale": Vector2(-1, -1)
	},
	
	1: {
		"position": Vector2(152, 200),
		"rotation": 0,
		"scale": Vector2(1, -1)
	},
	
	2: {
		"position": Vector2(168, 200),
		"rotation": 0,
		"scale": Vector2(-1, -1)
	},
	
	3: {
		"position": Vector2(288, 200),
		"rotation": 0,
		"scale": Vector2(1, -1)
	}
}

const form2 : Dictionary = {
	0: {
		"position": Vector2(0, 104),
		"rotation": 90,
		"scale": Vector2(-1, -1)
	},
	
	1: {
		"position": Vector2(320, 104),
		"rotation": 90,
		"scale": Vector2(-1, 1)
	},
	
	2: {
		"position": Vector2(0, 200),
		"rotation": 90,
		"scale": Vector2(1, -1)
	},
	
	3: {
		"position": Vector2(320, 200),
		"rotation": 90,
		"scale": Vector2(1, 1)
	}
}

const form3 : Dictionary = {
	0: {
		"position": Vector2(192, 80),
		"rotation": 25,
		"scale": Vector2(1, 1)
	},
	
	1: {
		"position": Vector2(128, 80),
		"rotation": 335,
		"scale": Vector2(-1, 1)
	},
	
	2: {
		"position": Vector2(128, 64),
		"rotation": 90,
		"scale": Vector2(-1, -1)
	},
	
	3: {
		"position": Vector2(192, 64),
		"rotation": 90,
		"scale": Vector2(-1, 1)
	},
	
	4: {
		"position": Vector2(128, 64),
		"rotation": 0,
		"scale": Vector2(-1, 1)
	},
	
	5: {
		"position": Vector2(192, 64),
		"rotation": 0,
		"scale": Vector2(1, 1)
	}
}

const form4 : Dictionary = {
	0: {
		"position": Vector2(0, 200),
		"rotation": 0,
		"scale": Vector2(-1, -1)
	},
	
	1: {
		"position": Vector2(160, 0),
		"rotation": 0,
		"scale": Vector2(1, 1)
	},
	
	2: {
		"position": Vector2(96, 200),
		"rotation": 0,
		"scale": Vector2(-1, -1)
	},
	
	3: {
		"position": Vector2(256, 0),
		"rotation": 0,
		"scale": Vector2(1, 1)
	},
	
	4: {
		"position": Vector2(192, 200),
		"rotation": 0,
		"scale": Vector2(-1, -1)
	},
	
	5: {
		"position": Vector2(352, 0),
		"rotation": 0,
		"scale": Vector2(1, 1)
	}
}

const copy_positions : Array = [
	[Vector2(304, 16), Vector2(16, 16), \
	Vector2(16, 184), Vector2(304, 184)],
	
	[Vector2(48, 48), Vector2(112, 80), \
	Vector2(208, 80), Vector2(272, 48)],
	
	[Vector2(64, 168), Vector2(128, 168), \
	Vector2(192, 168), Vector2(256, 168)],
	
	[Vector2(160, 58), Vector2(160, 58), \
	Vector2(160, 58), Vector2(160, 58)]
]

var anim : String = "NULL"
var prev_stag : String = "NULL"
var mode : String = "spike"
var interpolation : bool = false
var active : bool = true
var battle : bool = true
var hp : int = MAX_HP
var act : int
var player_id : int
var time_start : int
var d : float
var delta_x : float
var slope : float
var mod : float = 1.0
var velocity : Vector2
var v_mod : Vector2
var spike_mod : Vector2
var player_pos : Vector2
var path : Vector2
var form : Dictionary
var copy : Array

signal hit

func set_id(id):
	player_id = id
	
func despawned():
	pick_mode("tentacles")
	
func copy_despawned(copy):
	if copy == get_parent().get_parent().copies[3]:
		pick_mode("moar_spike")
	
func stagger(object, direction):
	if object == self:
		hp -= 2
		active = false
		get_parent().get_node("health").rect_size.x = \
			HP_MARGIN - ((4.8) * (MAX_HP - hp))
			
		if hp == 0:
			print("yeetusfeetusdeleetus")
			
			act = 0
			rotation = 0
			velocity = Vector2(0, 0)
			position = CENTER
			mode = "NULL"
		elif hp == MAX_HP * 0.50:
			mode = "moar_spike"
			act = 0
			velocity = Vector2(0, 0)
			rotation = 0
		else:
			match mode:
				"spike":
					if act == 4:
						slope *= -1
						rotation *= -1
						
						match direction:
							"east":
								rotation += PI
								spike_mod = Vector2(-1, -1)
							"west":
								rotation += PI
								spike_mod = Vector2(-1, -1)
					elif act > 4:
						pick_mode("spike_int")
				"tentacles":
					pass
				"bounce":
					if direction == prev_stag:
						mod += 0.25
					else:
						mod = 1.0
						
					match direction:
						"north":
							v_mod.y = -2 * (abs(velocity.y) / mod)
						"east":
							v_mod.x = 2 * (abs(velocity.x) / mod)
						"south":
							v_mod.y = 2 * (abs(velocity.y) / mod)
						"west":
							v_mod.x = -2 * (abs(velocity.x) / mod)
						
					prev_stag = direction
	
func parried(object, pos):
	pass
			
func pick_mode(prev : String):
	velocity = Vector2(0, 0)
	act = 0
	
	if hp > MAX_HP * 0.90:
		match prev:
			"spike_int":
				mode = "tentacles"
			"spike_nat":
				mode = "spike"
	elif hp > MAX_HP * 0.80:
		match prev:
			"spike_int":
				mode = "tentacles"
			"spike_nat":
				if (OS.get_unix_time() - time_start) % 2:
					mode = "spike"
				else:
					mode = "tentacles"
			"tentacles":
				mode = "spike"
	elif hp > MAX_HP * 0.70:
		match prev:
			"spike_int":
				mode = "tentacles"
			"spike_nat":
				if (OS.get_unix_time() - time_start) % 2:
					mode = "spike"
				else:
					mode = "tentacles"
			"tentacles":
				mode = "spike"
	elif hp > MAX_HP * 0.50:
		match prev:
			"spike_int":
				mode = "tentacles"
			"spike_nat":
				if (OS.get_unix_time() - time_start) % 2:
					mode = "spike"
				else:
					mode = "tentacles"
			"tentacles":
				mode = "bounce"
			"bounce":
				mode = "spike"
	else:
		match prev:
			"spike_int":
				if (OS.get_unix_time() - time_start) % 2:
					mode = "moar_spike"
				else:
					mode = "tentacles"
			"spike_nat":
				if (OS.get_unix_time() - time_start) % 2:
					mode = "spike"
				else:
					mode = "moar_spike"
			"moar_spike":
				if (OS.get_unix_time() - time_start) % 2:
					mode = "spike"
				else:
					mode = "bounce"
			"tentacles":
				if (OS.get_unix_time() - time_start) % 2:
					mode = "spike"
				else:
					mode = "bounce"
			"bounce":
				if (OS.get_unix_time() - time_start) % 2:
					mode = "moar_spike"
				else:
					mode = "tentacles"
					
	mode = "spike"
			
func pick_form():
	if hp > MAX_HP * 0.80:
		if (OS.get_unix_time() - time_start) % 2:
			return form1
		else:
			return form2
	else:
		match (OS.get_unix_time() - time_start) % 4:
			0:
				return form1
			1:
				return form2
			2:
				return form3
			3:
				return form4
	
func pick_copy():
	if hp == MAX_HP * 0.50:
		return copy_positions[1]
	else:
		return copy_positions[(OS.get_unix_time() - time_start) % 4]

func _ready():
	time_start = OS.get_unix_time()
	
	$spin_timer.one_shot = true
	$blink_timer.one_shot = true
	$bounce_timer.one_shot = true
	$reactive_timer.one_shot = true
	
	$thad_collisionPolygon.disabled = true

func _process(delta):
	if battle:
		position += velocity * delta
		if interpolation:
			velocity.x = lerp(velocity.x, 0, 0.1)
			velocity.y = lerp(velocity.y, 0, 0.1)
		
		if anim != "NULL":
			$thad_sprite.play(anim)
			
		if $reactive_timer.is_stopped() and !active:
			active = true
			
		match mode:
			"spike":
				run_spike()
			"moar_spike":
				run_moar_spike()
			"tentacles":
				run_tentacles()
			"bounce":
				run_bounce()
	
func run_inputs(pos):
	player_pos = pos
	
func run_spike():
	match act:
		0:
			$spin_timer.start(SPIN_TIME)
			anim = "to_spike"
			spike_mod = Vector2(1, 1)
			rotation = 0
			act += 1
		1:
			rotate(PI / 8)
			
			if $spin_timer.is_stopped():
				anim = "spike"
				act += 1
		2:
			$thad_collisionPolygon.disabled = false
			$thad_collisionShape.disabled = true
			
			path = global_position - player_pos
			slope = path.y / path.x
			
			var angle : float = atan(path.x / path.y)
			
			if path.y < 0:
				rotation = -((PI / 2) + angle)
			else:
				rotation = (PI / 2) - angle
				
			d = STARTING_VELOCITY
				
			act += 1
		3:
			delta_x = sqrt(abs(pow(d, 2) / (pow(slope, 2) + 1)))
			
			if path.x > 0:
				delta_x *= -1
				
			delta_x *= -1
			
			velocity = Vector2(delta_x, delta_x * slope)
			
			d -= BACC_A
			
			if d <= 0:
				act += 1
		4:
			if d < MAX_VELOCITY:
				d += ACCELERATION
			
			delta_x = sqrt(abs(pow(d, 2) / (pow(slope, 2) + 1)))
			
			if path.x > 0:
				delta_x *= -1
				
			velocity = Vector2(delta_x, delta_x * slope)
			velocity *= spike_mod
			
			var tip : Vector2 = Vector2(0, 0)
			
			tip.x = global_position.x - (cos(rotation) * TIP_LENGTH)
			tip.y = global_position.y - (sin(rotation) * TIP_LENGTH)
			
			if d >= MAX_VELOCITY:
				if tip.x <= 4392:
					global_position.x = \
					4392 + (global_position.x - tip.x)
					velocity = Vector2(0, 0)
					act += 1
				elif tip.x >= 4696:
					global_position.x = \
					4696 + (global_position.x - tip.x)
					velocity = Vector2(0, 0)
					act += 1
					
				if tip.y <= 312:
					global_position.y = \
					312 + (global_position.y - tip.y)
					velocity = Vector2(0, 0)
					act += 1
				elif tip.y >= 512:
					global_position.y = \
					512 + (global_position.y - tip.y)
					velocity = Vector2(0, 0)
					act += 1
		_:
			act += 1
			
			if act > 40:
				$thad_collisionPolygon.disabled = true
				$thad_collisionShape.disabled = false
			
				pick_mode("spike_nat")

func run_tentacles():
	match act:
		0:
			$thad_collisionPolygon.disabled = true
			$thad_collisionShape.disabled = true
			$blink_timer.start(BLINK_TIME)
			anim = "blink"
			form = pick_form()
			rotation = 0
			act += 1
		1:
			if $blink_timer.is_stopped():
				$blink_timer.start(BLINK_TIME)
				anim = "unblink"
				position = CENTER
				act += 1
		2:
			if $blink_timer.is_stopped():
				$thad_collisionShape.disabled = false
				anim = "idle"
				act += 1
		3:
			position.y = lerp(position.y, CENTER.y - RISE, 0.1)
			
			if position.y <= CENTER.y - RISE + 2:
				act += 1
				
				if hp > MAX_HP * 0.90:
					act = 0
					mode = "spike"
		4:
			act += 1
			
			for i in form.size():
				get_parent().get_parent().tentacles[i] \
				.position = form[i]["position"]
				get_parent().get_parent().tentacles[i] \
				.rotation_degrees = form[i]["rotation"]
				get_parent().get_parent().tentacles[i] \
				.scale = form[i]["scale"]
				get_parent().get_parent().tentacles[i].spawn()
		5:
			pass
		_:
			pass

func run_bounce():
	match act:
		0:
			$bounce_timer.start(BOUNCE_TIME)
			prev_stag = "NULL"
			v_mod = Vector2(0, 0)
			mod = 1.0
			d = MAX_BOUNCE_VELOCITY
			act += 1
		1:
			path = position - player_pos
			slope = path.y / path.x
			act += 1
		2:
			var r : int = 12
			
			delta_x = sqrt(abs(pow(d, 2) / (pow(slope, 2) + 1)))
			if path.x > 0:
				delta_x *= -1
			
			if position.x - r <= 8:
				position.x = 8 + r
				v_mod.x = 2 * (abs(velocity.x) / 1.5)
			elif position.x + r >= 312:
				position.x = 312 - r
				v_mod.x = -2 * (abs(velocity.x) / 1.5)
			if position.y - r <= 8:
				position.y = 8 + r
				v_mod.y = 2 * (abs(velocity.y) / 1.5)
			elif position.y + r >= 192:
				position.y = 192 - r
				v_mod.y = -2 * (abs(velocity.y) / 1.5)
				
			velocity.x = lerp(velocity.x, delta_x, 0.05)
			velocity.y = lerp(velocity.y, delta_x * slope, 0.05)
			velocity += v_mod
			v_mod = Vector2(0, 0)
			
			if $blink_timer.is_stopped():
				$blink_timer.start(BLINK_TIME)
				act = 1
				
			if $bounce_timer.is_stopped():
				act += 1
		3:
			var r : int = 12
			
			velocity.x = lerp(velocity.x, 0, 0.1)
			velocity.y = lerp(velocity.y, 0, 0.1)
			
			if position.x - r <= 8 or position.x + r >= 312:
				velocity = Vector2(0, 0)
			if position.y - r <= 8 or position.y + r >= 192:
				velocity = Vector2(0, 0)
			
			if abs(velocity.x) <= 1 and abs(velocity.y) <= 1:
				act += 1
		4:
			velocity = Vector2(0, 0)
			pick_mode("bounce")

func run_moar_spike():
	for copy in get_parent().get_parent().copies:
		copy.player_pos = player_pos
				
	match act:
		0:
			$thad_collisionPolygon.disabled = true
			$thad_collisionShape.disabled = true
			$blink_timer.start(BLINK_TIME)
			anim = "blink"
			rotation = 0
			act += 1
		1:
			if $blink_timer.is_stopped():
				$blink_timer.start(BLINK_TIME)
				anim = "unblink"
				position = CENTER
				act += 1
		2:
			if $blink_timer.is_stopped():
				copy = pick_copy()
				anim = "idle"
				act += 1
		3:
			position.y = lerp(position.y, CENTER.y - RISE, 0.1)
			
			if position.y <= CENTER.y - RISE + 2:
				$thad_collisionShape.disabled = false
				act += 1
		4:
			get_parent().get_parent().copies[0]. \
			spawn(copy[0])
			act += 1
		24:
			get_parent().get_parent().copies[1]. \
			spawn(copy[1])
			act += 1
		44:
			get_parent().get_parent().copies[2]. \
			spawn(copy[2])
			act += 1
		64:
			get_parent().get_parent().copies[3]. \
			spawn(copy[3])
			act += 1
		65:
			pass
		_:
			act += 1

func _on_thad_body_entered(body):
	if body.get_instance_id() == player_id:
		if active:
			emit_signal("hit")
		else:
			$reactive_timer.start(GRACE)
